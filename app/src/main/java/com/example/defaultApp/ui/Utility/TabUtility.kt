package com.example.defaultApp.ui.Utility

import android.view.ViewTreeObserver
import android.widget.HorizontalScrollView
import com.example.defaultApp.ui.MAX_SHOW_TABS
import com.example.defaultApp.ui.home.AppDataModel
import com.google.android.material.tabs.TabLayout
import java.util.ArrayList

class TabUtility(
        var tabLayout: TabLayout,
        var tabScrollView: HorizontalScrollView
) {

    fun updateTabs(dataModel: AppDataModel) {
        var tabNames = ArrayList<String>()
        var selectedIndex = dataModel.selectedIndex
        for (i in 0 until dataModel.tabCount) {
            if (i < MAX_SHOW_TABS || i == dataModel.selectedIndex) {
                tabNames.add(dataModel.tabNames[i])
                if (i == dataModel.selectedIndex) {
                    selectedIndex = tabNames.size - 1
                }
            }
        }
        com.example.defaultApp.ui.updateTabs(this.tabLayout, tabNames, selectedIndex)

        this.tabScrollView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                tabScrollView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val tabWidth = tabLayout.width / tabLayout.tabCount
                val left = tabWidth * tabLayout.selectedTabPosition
                val right = tabWidth * (tabLayout.selectedTabPosition + 1) - tabScrollView.width
                if (left < tabScrollView.scrollX) {
                    tabScrollView.scrollTo(left, 0)
                } else if (tabScrollView.scrollX < right) {
                    tabScrollView.scrollTo(right, 0)
                }
            }
        })
    }
}