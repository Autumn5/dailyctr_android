package com.example.defaultApp.ui

const val DATA_FILE_NAME = "CountDataStore"
const val MAX_TABS = 30
const val MAX_SHOW_TABS = 10
const val DEFAULT_TABS = 3
const val MAX_QR_SIZE = 1024
