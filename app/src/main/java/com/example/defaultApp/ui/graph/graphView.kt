package com.example.defaultApp.ui.graph

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class graphView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var paint = Paint()
    private var graphArray = ArrayList<GraphPoint>()

    override fun onDraw(canvas: Canvas) {
        canvas.drawColor(Color.argb(255, 240, 240, 255))

        paint.strokeWidth = 2f
        paint.setColor(Color.argb(255, 0, 128, 255))
        canvas.drawLine(1f, 1f, 1f, canvas.height -2f, paint)
        canvas.drawLine(1f, canvas.height -2f, canvas.width -2f, canvas.height -2f, paint)

        if (0 < graphArray.size) {
            val minCount = GraphModel.minCount(graphArray).toFloat()
            val heightRate = 1f / (GraphModel.maxCount(graphArray) - minCount).toFloat()
            val widthRate = 1f / (graphArray.last().x)

            val lineWidth = 5f
            val width = canvas.width - lineWidth
            val height = canvas.height - lineWidth
            paint.strokeWidth = lineWidth
            paint.setColor(Color.argb(255, 0, 128, 255))

            fun countX(x: Float) : Float {
                return width * x * widthRate + lineWidth / 2
            }
            fun countY(y: Float) : Float {
                return height * (1 - (y - minCount) * heightRate) + lineWidth / 2
            }

            for (i in 0 until graphArray.size) {
                canvas.drawCircle(countX(graphArray[i].x), countY(graphArray[i].y),
                        lineWidth, paint)
                if (i < graphArray.size - 1) {
                    canvas.drawLine(
                        countX(graphArray[i].x), countY(graphArray[i].y),
                        countX(graphArray[i+1].x), countY(graphArray[i+1].y), paint)
                }
            }
        }
    }

    fun setGraphArray(array: ArrayList<GraphPoint>) {
        this.graphArray = array
        this.postInvalidate()
    }
}