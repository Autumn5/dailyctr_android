package com.example.defaultApp.ui

import java.math.BigDecimal

fun Double.dispString() : String {
    if (this > Math.floor(this)) {
        return (BigDecimal(Math.round(this * 100)).divide(100.toBigDecimal())).toString()
    }
    return this.toInt().toString()
}

fun Double.fix() : Double {
    if (this > Math.floor(this)) {
        return Math.round(this * 100) / 100.0
    }
    return this
}
