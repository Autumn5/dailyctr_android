package com.example.defaultApp.ui.tablist

import android.util.Log
import com.example.defaultApp.ui.convStrTime
import com.example.defaultApp.ui.dispString
import com.example.defaultApp.ui.home.AppDataModel
import com.example.defaultApp.ui.home.TabData
import com.example.defaultApp.ui.stringFromDate
import java.text.Normalizer
import java.util.*

class TablistViewModel {
    companion object {
        // 指定タブの先頭行のカウント文字列を返す
        fun firstRowCountText(tab: TabData, dataModel: AppDataModel): String {
            var count = 0.0
            if (0 < tab.dailyList.size) {
                count = dataModel.dailyList[tab.dailyList.last()].count
            }

            if (tab.isTimeMode) {
                return convStrTime(count.toInt())
            }
            return count.dispString()
        }

        // 指定タブの先頭行の日付文字列を返す
        fun firstRowDateText(tab: TabData, dataModel: AppDataModel): String {
            val strToday = stringFromDate(Date())
            var strDate = ""
            if (0 < tab.dailyList.size) {
                strDate = dataModel.dailyList[tab.dailyList.last()].strDate
            }
            if (strToday == strDate) {
                return "今日"
            }
            if (8 <= strDate.length && strToday.substring(0, 5) == strDate.substring(0, 5)) {
                return strDate.substring(5)
            }
            return strDate
        }

        // 絞り込みテキストよりタブIndexリストを作成
        fun createRowList(list: List<String>, filterText: String): ArrayList<Int> {
            val rowList = ArrayList<Int>()
            for (idx in 0 until list.size) {
                if (filterText.equals("") ||
                        list[idx].contains(filterText, ignoreCase = true)) {
                    rowList.add(idx)
                }
            }
            return rowList
        }

    }
}