package com.example.defaultApp.ui.qr

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.defaultApp.R
import com.example.defaultApp.ui.home.AppDataModel
import com.example.defaultApp.ui.simpleAlert
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CompoundBarcodeView

const val REQUEST_CAMERA_PERMISSION = 0

class QrReadActivity : AppCompatActivity() {
    lateinit var qr_view: CompoundBarcodeView
    lateinit var dataModel: AppDataModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr_read)
        this.qr_view = findViewById(R.id.qr_view)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        this.dataModel = AppDataModel(this)
        dataModel.loadVariable()
        this.initQRCamera()
    }

    private fun initQRCamera() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            openQRCamera()
        }
    }

    private fun openQRCamera() {
        qr_view.resume()
        val context = this
        qr_view.decodeContinuous(object : BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                if (result != null) {
                    qr_view.pause()
                    val isSuccess = dataModel.importQR(result.text)
                    if (isSuccess) {
                        dataModel.saveVariable()
                    }
                    AlertDialog.Builder(context)
                            .setTitle("QR読み込み")
                            .setMessage(if (isSuccess) "成功しました" else "失敗しました")
                            .setNegativeButton("OK", { dialog, which ->
                                finish()
                            }).show()
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) { }
        })
    }

    // actions

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}