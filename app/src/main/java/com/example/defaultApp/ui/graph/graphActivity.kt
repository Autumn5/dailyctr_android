package com.example.defaultApp.ui.graph

import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.defaultApp.R
import com.example.defaultApp.ui.*
import com.example.defaultApp.ui.home.AppDataModel
import com.example.defaultApp.ui.home.CountTermType
import com.google.android.material.tabs.TabLayout
import kotlin.collections.ArrayList


class graphActivity : AppCompatActivity() {
    lateinit var graphView: graphView
    lateinit var yMaxTextView: TextView
    lateinit var yMinTextView: TextView
    lateinit var xMaxTextView: TextView
    lateinit var xMinTextView: TextView
    lateinit var tabLayout: TabLayout
    lateinit var dataModel: AppDataModel
    var countTerm = CountTermType.TERM_DAYLY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graph)
        this.graphView = findViewById<graphView>(R.id.graphView)
        this.yMaxTextView = findViewById<TextView>(R.id.yMaxTextView)
        this.yMinTextView = findViewById<TextView>(R.id.yMinTextView)
        this.xMaxTextView = findViewById<TextView>(R.id.xMaxTextView)
        this.xMinTextView = findViewById<TextView>(R.id.xMinTextView)
        this.tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        this.countTerm = intent.getSerializableExtra("COUNT_TERM") as CountTermType
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)

        this.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                countTerm = CountTermType.values().find { it.rawValue == tabLayout.selectedTabPosition }!!
                updateDisplay()
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        this.dataModel = AppDataModel(this)
        this.dataModel.loadVariable()
        updateDisplay()
    }

    // actions

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // functions

    fun updateDisplay() {
        var graphArray : ArrayList<GraphPoint>
        if (countTerm == CountTermType.TERM_DAYLY) {
            graphArray = GraphModel.createGraphDailyArray(dataModel)
        } else if (countTerm == CountTermType.TERM_WEEKLY) {
            graphArray = GraphModel.createGraphWeekArray(dataModel)
        } else {
            graphArray = GraphModel.createGraphMonthArray(dataModel)
        }
        this.tabLayout.selectTab(this.tabLayout.getTabAt(countTerm.rawValue))
        yMaxTextView.text = GraphModel.yMaxText(this.dataModel, graphArray)
        yMinTextView.text = GraphModel.yMinText(this.dataModel, graphArray)
        xMaxTextView.text = graphArray.lastOrNull()?.xLabel ?: ""
        xMinTextView.text = graphArray.firstOrNull()?.xLabel ?: ""
        graphView.setGraphArray(graphArray)
    }

}