package com.example.defaultApp.ui.Utility

import android.app.Activity
import android.graphics.Rect
import android.util.TypedValue
import android.view.ViewGroup
import android.view.ViewTreeObserver

class KeyboardVisibility(
        private val activity: Activity
) {

    interface OnKeyboardVisibilityListener {
        fun onVisibilityChanged(visible: Boolean)
    }

    fun setKeyboardVisibilityListener(listener: OnKeyboardVisibilityListener) {
        val parentView = (this.activity.findViewById(android.R.id.content) as ViewGroup).getChildAt(0)
        parentView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            private var alreadyOpen = false
            private val EstimatedKeyboardDP = 100
            private val rect = Rect()
            override fun onGlobalLayout() {
                val estimatedKeyboardHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                        EstimatedKeyboardDP.toFloat(), parentView.resources.displayMetrics).toInt()
                parentView.getWindowVisibleDisplayFrame(rect)
                val heightDiff = parentView.rootView.height - rect.height()
                val isShown = heightDiff >= estimatedKeyboardHeight
                if (isShown == alreadyOpen) {
                    return
                }
                alreadyOpen = isShown
                listener.onVisibilityChanged(isShown)
            }
        })
    }

}