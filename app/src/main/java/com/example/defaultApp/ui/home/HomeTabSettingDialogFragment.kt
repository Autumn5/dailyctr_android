package com.example.defaultApp.ui.home

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.defaultApp.R
import com.example.defaultApp.ui.backColor

enum class CountModeType(val rawValue: Int) {
    COUNT(0), TIME(1), UPDATE(2);

    companion object {
        fun init(row: Int) : CountModeType {
            return values().filter { row == it.rawValue }.first()
        }
    }
}

enum class CulcModeType(val rawValue: Int) {
    TOTAL(0), AVG(1), MAX(2);

    companion object {
        fun init(row: Int) : CulcModeType {
            return values().filter { row == it.rawValue }.first()
        }
    }
}

class HomeTabSettingDialogFragment(private val dataModel: AppDataModel)
    : DialogFragment() {
    lateinit var tabNameEditText : EditText
    lateinit var modeSpinner : Spinner
    lateinit var culcSpinner : Spinner
    lateinit var backColorSpinner : Spinner
    lateinit var listener: OnClickHomeTabSettingDialogListener
    lateinit var colors: Array<Int>

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater;
            val view = inflater.inflate(R.layout.fragment_home_tab_setting, null)

            this.tabNameEditText = view.findViewById<EditText>(R.id.settingTabNameEditText)
            this.modeSpinner = view.findViewById<Spinner>(R.id.modeSpinner)
            this.culcSpinner = view.findViewById<Spinner>(R.id.culcSpinner)
            this.backColorSpinner = view.findViewById<Spinner>(R.id.backColorSpinner)

            this.tabNameEditText.setText(this.dataModel.tabName, TextView.BufferType.NORMAL)

            this.setItems(modeSpinner, arrayOf("カウント", "時間", "更新"))
            this.modeSpinner.setSelection(this.countMode().rawValue)
            this.modeSpinner.isEnabled = (this.dataModel.startDate == null)

            this.setItems(this.culcSpinner, arrayOf("合計", "平均"))
            this.culcSpinner.setSelection(this.culcMode().rawValue)

            this.colors = arrayOf(0,
                    Color.parseColor("#FFC0C0"),
                    Color.parseColor("#FFFFC0"),
                    Color.parseColor("#C0FFC0"),
                    Color.parseColor("#C0FFFF"),
                    Color.parseColor("#C0C0FF"),
                    Color.parseColor("#FFC0FF"))
            this.setItems(this.backColorSpinner
                    , arrayOf("デフォルト", "赤", "黄色", "緑", "水色", "青", "桃色")
                    , this.colors)
            this.backColorSpinner.setSelection(this.colorPosition())

            builder.setView(view)
            builder.setPositiveButton(resources.getString(R.string.title_update),
                            DialogInterface.OnClickListener { dialog, id ->
                                this.onTapUpdateButton()
                            })
            builder.setNegativeButton(resources.getString(R.string.title_cancel), null)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    // actions

    fun onTapUpdateButton() {
        this.dataModel.tabName = this.tabNameEditText.text.toString()
        when(CountModeType.init(this.modeSpinner.selectedItemPosition)) {
            CountModeType.COUNT -> {
                this.dataModel.isTimeMode = false
                this.dataModel.isUpdateMode = false
            }
            CountModeType.TIME -> {
                this.dataModel.isTimeMode = true
                this.dataModel.isUpdateMode = false
            }
            CountModeType.UPDATE -> {
                this.dataModel.isTimeMode = false
                this.dataModel.isUpdateMode = true
            }
        }
        when(CulcModeType.init(this.culcSpinner.selectedItemPosition)) {
            CulcModeType.TOTAL -> {
                this.dataModel.isCulcAvg = false
            }
            CulcModeType.AVG -> {
                this.dataModel.isCulcAvg = true
            }
        }
        this.dataModel.backColor = this.colors[this.backColorSpinner.selectedItemPosition]
        this.dataModel.saveVariable()
        this.listener.onTapHomeTabSettingDialogUpdateButton()
    }

    // functions

    private fun countMode() : CountModeType {
        if (this.dataModel.isTimeMode) {
            return CountModeType.TIME
        }
        if (this.dataModel.isUpdateMode) {
            return CountModeType.UPDATE
        }
        return CountModeType.COUNT
    }

    private fun culcMode() : CulcModeType {
        if (this.dataModel.isCulcAvg) {
            return CulcModeType.AVG
        }
        return CulcModeType.TOTAL
    }

    private fun colorPosition() : Int {
        val position = this.colors.indexOf(this.dataModel.backColor)
        if (0 <= position) {
            return position
        }
        return 0
    }

    private fun setItems(spinner: Spinner, spinnerItems: Array<String>) {
        val adapter = ArrayAdapter(
                requireActivity(),
                android.R.layout.simple_spinner_item,
                spinnerItems
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.setAdapter(adapter)
    }

    private fun setItems(spinner: Spinner, spinnerItems: Array<String>, colors: Array<Int>) {
        val adapter = ColorArrayAdapter(
                requireActivity(),
                android.R.layout.simple_spinner_item,
                android.R.layout.simple_spinner_dropdown_item,
                spinnerItems,
                colors
        )
        spinner.setAdapter(adapter)
    }

    // listener

    interface OnClickHomeTabSettingDialogListener {
        fun onTapHomeTabSettingDialogUpdateButton()
    }

    fun setOnClickHomeTabSettingDialogListener(listener: OnClickHomeTabSettingDialogListener){
        this.listener = listener
    }

    // ArrayAdapter

    private class ColorArrayAdapter(
            context: Context,
            val resourceId: Int,
            val dropDownResourceId: Int,
            items: Array<String>,
            val colors: Array<Int>)
        : ArrayAdapter<String?>(context, resourceId, items)
    {
        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View? {
            var convertView = convertView
            if (convertView == null) {
                val inflater = context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(this.dropDownResourceId, null)
            }
            val text1 = convertView!!.findViewById<TextView>(android.R.id.text1)
            text1.text = getItem(position)
            convertView.setBackgroundColor(backColor(this.context, this.colors[position]))
            text1.minHeight = 80
            return convertView
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView: View? = convertView
            if (convertView == null) {
                val inflater = context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                convertView = inflater.inflate(this.resourceId, null)
            }
            val text1 = convertView!!.findViewById<TextView>(android.R.id.text1)
            text1.text = getItem(position)
            convertView.setBackgroundColor(backColor(this.context, this.colors[position]))
            return convertView
        }
    }


}