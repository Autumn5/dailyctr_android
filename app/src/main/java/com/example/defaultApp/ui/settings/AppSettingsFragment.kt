package com.example.defaultApp.ui.settings

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.defaultApp.BuildConfig
import com.example.defaultApp.R
import com.example.defaultApp.ui.*
import com.example.defaultApp.ui.home.AppDataModel

const val COUNT_LOG_FILE_NAME = "counterLog.csv"
const val IMAGE_FILE_NAME = "background.jpg"
const val IMPORT_COUNT_LOG = 0
const val EXPORT_COUNT_LOG = 1
const val LOAD_IMAGE = 2

class AppSettingsFragment : Fragment() {
    lateinit var dataModel: AppDataModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)
        val countLogLoadButton = root.findViewById<Button>(R.id.countLogLoadButton)
        val countLogSaveButton = root.findViewById<Button>(R.id.countLogSaveButton)
        val countLogPathTextView = root.findViewById<TextView>(R.id.countLogPathTextView)
        val imageLoadButton = root.findViewById<Button>(R.id.imageLoadButton)
        val imageDeleteButton = root.findViewById<Button>(R.id.imageDeleteButton)
        val versionTextView = root.findViewById<TextView>(R.id.versionTextView)
        val privacyButton = root.findViewById<Button>(R.id.privacyButton)
        this.dataModel = AppDataModel(this.requireActivity())

        countLogPathTextView.text = COUNT_LOG_FILE_NAME
        versionTextView.text = BuildConfig.VERSION_NAME.toString()
        removeActionBarLayout(this.requireActivity())

        countLogLoadButton.setOnClickListener {
            this.onClickCountLogLoadButton()
        }

        countLogSaveButton.setOnClickListener{
            this.onClickCountLogSaveButton()
        }

        imageLoadButton.setOnClickListener {
            this.onClickImageLoadButton()
        }

        imageDeleteButton.setOnClickListener {
            this.onClickImageDeleteButton()
        }

        privacyButton.setOnClickListener{
            val uri = Uri.parse("https://hp.vector.co.jp/authors/VA021917/DailyCTR/PrivacyPolicy.html")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
        return root
    }

    // actions

    fun onClickCountLogLoadButton() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/*"
            putExtra(Intent.EXTRA_TITLE, COUNT_LOG_FILE_NAME)
        }
        startActivityForResult(intent, IMPORT_COUNT_LOG)
    }

    fun onClickCountLogSaveButton() {
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            type = "text/csv"
            putExtra(Intent.EXTRA_TITLE, COUNT_LOG_FILE_NAME)
        }
        startActivityForResult(intent, EXPORT_COUNT_LOG)
    }

    fun onClickImageLoadButton() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "image/*"
        }
        startActivityForResult(intent, LOAD_IMAGE)
    }

    fun onClickImageDeleteButton() {
        deleteImage(this.requireActivity(), IMAGE_FILE_NAME)
        simpleAlert(this.requireActivity(), "背景イメージ", "削除しました")
    }

    // functions

    fun importCountLog(uri : Uri) {
        val stream = this.requireActivity().contentResolver.openInputStream(uri)!!
        val csvArray = loadStringArray(stream)
        this.dataModel.loadVariable()
        val isSuccess = this.dataModel.importCSV(csvArray)
        if (isSuccess) {
            this.dataModel.saveVariable()
        }
        simpleAlert(this.requireActivity(), "カウント履歴",
            if (isSuccess) { "読み込みました" } else { "読み込みに失敗しました" })
    }

    fun exportCountLog(uri : Uri) {
        this.dataModel.loadVariable()
        val stream = this.requireActivity().contentResolver.openOutputStream(uri)!!
        saveStringArray(stream, this.dataModel.exportCSV())

        simpleAlert(this.requireActivity(), "カウント履歴", "保存しました")
    }

    fun loadImage(uri: Uri) {
        var isSuccess = false
        loadImage(this.requireActivity(), uri, 640)?.let {
            if (saveImage(this.requireActivity(), IMAGE_FILE_NAME, it)) {
                isSuccess = true
            }
        }

        simpleAlert(this.requireActivity(), "背景イメージ",
                if (isSuccess) { "読み込みました" } else { "読み込みに失敗しました" })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode != Activity.RESULT_OK || intent == null) {
            return
        }
        if (requestCode == IMPORT_COUNT_LOG) {
            this.importCountLog(intent.data!!)
        } else if (requestCode == EXPORT_COUNT_LOG) {
            this.exportCountLog(intent.data!!)
        } else if (requestCode == LOAD_IMAGE) {
            this.loadImage(intent.data!!)
        }
    }

}