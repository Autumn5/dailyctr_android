package com.example.defaultApp.ui.item

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import com.example.defaultApp.R
import com.example.defaultApp.ui.home.AppDataModel

enum class ItemViewType {
    header,
    rowData,
}

class MyItemRecyclerViewAdapter(
        private val dataModel: AppDataModel
)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    lateinit var listener: OnItemClickListener
    var isShowDetail = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == ItemViewType.header.ordinal) {
            return onCreateHeaderViewHolder(parent)
        }
        return onCreateRowDataViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == ItemViewType.header.ordinal){
            onBindHeaderViewHolder(holder as HeaderViewHolder)
            return
        }
        onBindRowDataViewHolder(holder as RowDataViewHolder, position - 1)
    }

    override fun getItemCount(): Int {
        if (0 < dataModel.dailyTabListSize()) {
            return dataModel.dailyTabListSize() + 1
        }
        return 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            ItemViewType.header.ordinal
        } else ItemViewType.rowData.ordinal
    }

    // view - header

    fun onCreateHeaderViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_item_header, parent, false)
        return HeaderViewHolder(view)
    }

    fun onBindHeaderViewHolder(holder: HeaderViewHolder) {
        holder.itemView.setOnClickListener {
            this.isShowDetail = !this.isShowDetail
            notifyDataSetChanged()
        }

        val dateInfo = dataModel.dateInfoOfAll()
        holder.countInfoTextView.text = ItemViewModel.headerCountText(dataModel, dateInfo)

        if (!isShowDetail) {
            holder.dateTermView.visibility = GONE
            holder.totalAvgInfoView.visibility = GONE
            holder.minMaxInfoView.visibility = GONE
            return
        }

        holder.dateTermView.visibility = VISIBLE
        holder.dateTermTextView.text = ItemViewModel.headerDateTermText(dateInfo)

        if (dataModel.isCulcAvg) {
            holder.totalInfoView.visibility = GONE
        } else {
            holder.totalInfoView.visibility = VISIBLE
            holder.totalCountTextView.text = AppDataModel.countText(dataModel.totalCount, dataModel.isTimeMode)
        }
        holder.totalAvgInfoView.visibility = VISIBLE
        holder.avgCountTextView.text = AppDataModel.countText(dataModel.averageOfAll(), dataModel.isTimeMode)

        val minText = AppDataModel.countText(dataModel.minOfAll(), dataModel.isTimeMode)
        val maxText = AppDataModel.countText(dataModel.maxOfAll(), dataModel.isTimeMode)
        if (minText != maxText) {
            holder.minCountTextView.text = minText
            holder.maxCountTextView.text = maxText
            holder.minMaxInfoView.visibility = VISIBLE
        } else {
            holder.minMaxInfoView.visibility = GONE
        }
    }

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val countInfoTextView = view.findViewById<TextView>(R.id.item_header_countTextView)
        val dateTermTextView = view.findViewById<TextView>(R.id.item_header_dateTermTextView)
        val dateTermView = view.findViewById<View>(R.id.item_header_dateTermView)
        val totalCountTextView = view.findViewById<TextView>(R.id.item_header_totalCountTextView)
        val totalInfoView = view.findViewById<View>(R.id.item_header_totalInfoView)
        val avgCountTextView = view.findViewById<TextView>(R.id.item_header_avgCountTextView)
        val totalAvgInfoView = view.findViewById<View>(R.id.item_header_totalAvgInfoView)
        val minCountTextView = view.findViewById<TextView>(R.id.item_header_minCountTextView)
        val maxCountTextView = view.findViewById<TextView>(R.id.item_header_maxCountTextView)
        val minMaxInfoView = view.findViewById<View>(R.id.item_header_minMaxInfoView)
    }

    // view - rowData

    fun onCreateRowDataViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_item, parent, false)
        return RowDataViewHolder(view)
    }

    fun onBindRowDataViewHolder(holder: RowDataViewHolder, position: Int) {
        val idx = dataModel.dailyTabListSize() - position - 1
        holder.textView.text = dataModel.dailyTabList(idx).strDate +
                " " + dataModel.dailyTabListCountText(idx)

        holder.itemView.setOnClickListener {
            this.listener.onItemClickListener(idx)
        }
    }

    inner class RowDataViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView = view.findViewById<TextView>(R.id.item)
    }

    // functions - click rows

    interface OnItemClickListener {
        fun onItemClickListener(dataIndex: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener){
        this.listener = listener
    }

}