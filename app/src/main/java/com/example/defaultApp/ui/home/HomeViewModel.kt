package com.example.defaultApp.ui.home

import com.example.defaultApp.ui.*
import java.util.*

class HomeViewModel {
    companion object {

        fun strTime(dataModel: AppDataModel) : String {
            return dataModel.startDate?.let {
                stringFromDate(it, "HH:mm")
            } ?: dataModel.strTime
        }

        fun timeCount(dataModel: AppDataModel) : Int {
            return diffSeconds(dataModel.startDate!!, Date())
        }

        // 昨日タイトルを返す
        fun yesterdayTitle(countTerm: CountTermType) : String {
            if (countTerm == CountTermType.TERM_MONTHLY) {
                return "今月"
            } else if (countTerm == CountTermType.TERM_WEEKLY) {
                return "今週"
            } else {
                return "昨日"
            }
        }

        //  合計タイトルを返す
        fun totalTitle(dataModel: AppDataModel, countTerm: CountTermType) : String {
            if (countTerm == CountTermType.TERM_MONTHLY) {
                return "先月"
            } else if (countTerm == CountTermType.TERM_WEEKLY) {
                return "先週"
            } else {
                if (dataModel.isCulcAvg) {
                    return "平均"
                }
                return "合計"
            }
        }

        // 今日のカウント表示文字列を返す
        fun todayCountText(dataModel: AppDataModel) : String {
            if (dataModel.isTimeMode) {
                if (dataModel.startDate != null) {
                    return convStrTime(HomeViewModel.timeCount(dataModel))
                }
                return convStrTime(dataModel.count.toInt())
            } else {
                return dataModel.count.dispString()
            }
        }

        // 集計カウント表示文字列を返す
        // return 表示文字列配列(昨日カウント, 合計カウント)
        fun sumCountTexts(dataModel: AppDataModel, countTerm: CountTermType) : List<String> {
            var yesterdayCount = 0.0
            var totalCount = 0.0
            if (countTerm == CountTermType.TERM_MONTHLY) {
                val counts = dataModel.countOfMonth()
                yesterdayCount = counts.first
                totalCount = counts.second
            } else if (countTerm == CountTermType.TERM_WEEKLY) {
                val counts = dataModel.countOfWeek()
                yesterdayCount = counts.first
                totalCount = counts.second
            } else {
                yesterdayCount = dataModel.yesterDayCount
                totalCount = dataModel.countOfAll()
            }

            var yesterdayCountText = ""
            var totalCountText = ""
            if (dataModel.isTimeMode) {
                yesterdayCountText = convStrTime(yesterdayCount.toInt())
                totalCountText = convStrTime(totalCount.toInt())
            } else {
                yesterdayCountText = yesterdayCount.dispString()
                totalCountText = totalCount.dispString()
            }
            return listOf(yesterdayCountText, totalCountText)
        }
    }
}