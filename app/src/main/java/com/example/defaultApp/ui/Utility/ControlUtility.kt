package com.example.defaultApp.ui

import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.util.TypedValue
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import com.google.android.material.tabs.TabLayout

//
// Utility 21/1/24(v0.0.1)
//

// Utility methods - TabLayout

fun removeTabs(tabLayout: TabLayout, maxTabs: Int) {
    if (maxTabs < tabLayout.tabCount) {
        val removeCount = tabLayout.tabCount - maxTabs
        for (i in 0 until removeCount) {
            tabLayout.removeTabAt(maxTabs)
        }
    }
}

fun updateTabs(tabLayout: TabLayout, tabNames:List<String>, selectedIndex: Int) {
    for (i in 0 until tabNames.size) {
        if (tabLayout.tabCount <= i) {
            tabLayout.addTab(tabLayout.newTab())
        }
        tabLayout.getTabAt(i)?.text = tabNames[i]
    }
    removeTabs(tabLayout, tabNames.size)
    tabLayout.selectTab(tabLayout.getTabAt(selectedIndex))
}

// Utility methods - Activity

fun setActionBarTitle(context: Context, title: String) {
    val compatActivity = context as AppCompatActivity
    compatActivity.getSupportActionBar()?.setTitle(title)
}

fun isDarkTheme(context: Context) : Boolean {
    val uiMode = context.resources.configuration.uiMode
    return ((uiMode and Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES)
}

// 背景色を返す
// @param color 0=デフォルト背景色に変換
fun backColor(context: Context, color: Int) : Int {
    if (color == 0) {
        val typedValue = TypedValue()
        if (context.getTheme().resolveAttribute(android.R.attr.colorBackground, typedValue, true)) {
            return typedValue.data
        }
    }
    if (isDarkTheme(context)) {
        return Color.rgb((color.red + 16) / 2, (color.green + 16) / 2, (color.blue + 16) / 2)
    }
    return color
}

fun setActionBarLayout(context: Context, layoutResource: Int, isShowTitle: Boolean): View {
    val compatActivity = context as AppCompatActivity
    val inflater = compatActivity.layoutInflater
    val actionBarLayout = inflater.inflate(layoutResource, null)
    compatActivity.getSupportActionBar()?.setDisplayShowCustomEnabled(true)
    compatActivity.getSupportActionBar()?.setDisplayShowTitleEnabled(isShowTitle)
    compatActivity.getSupportActionBar()?.setCustomView(actionBarLayout)
    return actionBarLayout
}

fun removeActionBarLayout(context: Context) {
    val compatActivity = context as AppCompatActivity
    compatActivity.getSupportActionBar()?.setDisplayShowCustomEnabled(false)
    compatActivity.getSupportActionBar()?.setDisplayShowTitleEnabled(true)
}

// Utility methods - 属性テキスト

fun sizeSpanText(text: String, relativeText: String, relativeSize: Float): SpannableStringBuilder {
    val sb = SpannableStringBuilder()
    sb.append(text)
    val start = sb.length
    sb.append(relativeText)
    sb.setSpan(RelativeSizeSpan(relativeSize), start, sb.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    return sb
}
