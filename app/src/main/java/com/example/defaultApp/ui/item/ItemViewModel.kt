package com.example.defaultApp.ui.item

import android.view.View
import com.example.defaultApp.ui.dispString
import com.example.defaultApp.ui.home.AppDataModel

class ItemViewModel {
    companion object {
        // ヘッダー：件数・平均日数
        fun headerCountText(dataModel: AppDataModel, dateInfo: AppDataModel.DateInfo) : String {
            if (dataModel.dailyTabListSize() == 0) {
                return ""
            }
            val avgDays = dateInfo.diffDays.toDouble() / dataModel.dailyTabListSize().toDouble()
            return String.format("%d件(平均%s日/件)"
                    , dataModel.dailyTabListSize()
                    , avgDays.dispString())
        }

        // ヘッダー：日付
        fun headerDateTermText(dateInfo: AppDataModel.DateInfo) : String {
            return String.format("%s〜%s", dateInfo.minStrDate, dateInfo.maxStrDate)
        }

    }
}