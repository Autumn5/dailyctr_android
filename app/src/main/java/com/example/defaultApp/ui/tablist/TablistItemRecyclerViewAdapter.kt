package com.example.defaultApp.ui.tablist

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.defaultApp.R
import com.example.defaultApp.ui.dispString
import com.example.defaultApp.ui.home.AppDataModel
import com.example.defaultApp.ui.normalize
import kotlin.collections.ArrayList

class TablistItemRecyclerViewAdapter(
    private val dataModel: AppDataModel,
    private var filterText: String
)
    : RecyclerView.Adapter<TablistItemRecyclerViewAdapter.ViewHolder>() {
    lateinit var listener: OnItemClickListener
    var rowList: ArrayList<Int> = ArrayList()
    var filterList: MutableList<String>
    var isEditMode: Boolean = false

    init {
        this.filterList = dataModel.tabNames.map {normalize(it)}
                .subList(0, dataModel.tabCount).toMutableList()
        this.createRowList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_tablist_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val idx = this.rowList[position]
        val tab = dataModel.tabDatas[idx]
        holder.textView.text = tab.tabName
        holder.countTextView.text = TablistViewModel.firstRowCountText(tab, dataModel)
        holder.dateTextView.text = TablistViewModel.firstRowDateText(tab, dataModel)

        if (this.isEditMode) {
            holder.rowCountButton.visibility = GONE
            holder.coverButton.visibility = GONE
            holder.sortImageView.visibility = VISIBLE
        } else {
            if (tab.isTimeMode || tab.isUpdateMode) {
                holder.rowCountButton.visibility = INVISIBLE
                holder.coverButton.visibility = INVISIBLE
            } else {
                holder.rowCountButton.visibility = VISIBLE
                holder.coverButton.visibility = VISIBLE
                holder.rowCountButton.text = "+" + tab.increment.dispString()
            }
            holder.sortImageView.visibility = GONE
        }

        holder.itemView.setOnClickListener {
            if (isEditMode) {
                return@setOnClickListener
            }
            this.listener.onItemClickListener(idx)
        }
        holder.rowCountButton.setOnClickListener {
            this.listener.onTapCountButton(idx)
        }

        holder.sortImageView.setOnTouchListener { view, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                this.listener.startDragging(holder)
            }
            true
        }
    }

    override fun getItemCount(): Int = this.rowList.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView = view.findViewById<TextView>(R.id.tablist_item)
        val countTextView = view.findViewById<TextView>(R.id.tablist_count)
        val dateTextView = view.findViewById<TextView>(R.id.tablist_date)
        val rowCountButton = view.findViewById<Button>(R.id.rowCountButton)
        val coverButton = view.findViewById<Button>(R.id.coverButton)
        val sortImageView = view.findViewById<ImageView>(R.id.sortImageView)
    }

    // functions - click rows

    interface OnItemClickListener {
        fun onItemClickListener(dataIndex: Int)
        fun onTapCountButton(dataIndex: Int)
        fun startDragging(viewHolder: RecyclerView.ViewHolder)
    }

    fun setOnItemClickListener(listener: OnItemClickListener){
        this.listener = listener
    }

    fun setFilterText(filterText: String){
        this.filterText = filterText
        this.createRowList()
        notifyDataSetChanged()
    }

    // functions

    fun toggleEditMode() {
        this.isEditMode = !this.isEditMode
        notifyDataSetChanged()
    }

    fun createRowList() {
        this.rowList = TablistViewModel.createRowList(this.filterList, normalize(filterText))
    }

    fun moveFilterList(fromIndex: Int, toIndex: Int) {
        val moveRow = this.filterList[fromIndex]
        this.filterList.removeAt(fromIndex)
        this.filterList.add(toIndex, moveRow)
    }

}