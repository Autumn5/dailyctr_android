package com.example.defaultApp.ui.home

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.defaultApp.R
import com.example.defaultApp.ui.*
import java.util.*
import kotlin.collections.ArrayList

data class TabData(
        var count: Double = 0.0,
        var yesterDayCount: Double = 0.0,
        var totalCount: Double = 0.0,
        var strDate: String = "",
        var strTime: String = "-",
        var increment: Double = 1.0,
        var incrementHistories: ArrayList<Double> = ArrayList<Double>(),
        var tabName: String = "",
        var isTimeMode: Boolean = false,
        var isCulcAvg: Boolean = false,
        var isUpdateMode: Boolean = false,
        var startDate: Date? = null,
        var backColor: Int = 0,
        var dailyList: ArrayList<Int> = ArrayList<Int>()) {

    // カウント表示文字列を返す
    fun countText() : String {
        if (this.isTimeMode) {
            return convStrTime(this.count.toInt())
        }
        return this.count.dispString()
    }

}

data class DailyData(
        var count: Double = 0.0,
        var strDate: String = "",
        var tab: Int = 0) {}

class AppDataModel(
        private val context: Context?
) {
    var selectedIndex = 0
    var tabCount = DEFAULT_TABS
    var tabDatas = ArrayList<TabData>()
    var dailyList = ArrayList<DailyData>()

    var count: Double
        get() { return this.tabDatas[selectedIndex].count }
        set(value) { this.tabDatas[selectedIndex].count = value }
    var yesterDayCount: Double
        get() { return this.tabDatas[selectedIndex].yesterDayCount }
        set(value) { this.tabDatas[selectedIndex].yesterDayCount = value }
    var totalCount: Double
        get() { return this.tabDatas[selectedIndex].totalCount }
        set(value) { this.tabDatas[selectedIndex].totalCount = value }
    var strDate: String
        get() { return this.tabDatas[selectedIndex].strDate }
        set(value) { this.tabDatas[selectedIndex].strDate = value }
    var strTime: String
        get() { return this.tabDatas[selectedIndex].strTime }
        set(value) { this.tabDatas[selectedIndex].strTime = value }
    var increment: Double
        get() { return this.tabDatas[selectedIndex].increment }
        set(value) { this.tabDatas[selectedIndex].increment = value }
    var incrementHistories: ArrayList<Double>
        get() { return this.tabDatas[selectedIndex].incrementHistories }
        set(value) { this.tabDatas[selectedIndex].incrementHistories = value }
    var tabName: String
        get() { return this.tabDatas[selectedIndex].tabName }
        set(value) { this.tabDatas[selectedIndex].tabName = value }
    var isTimeMode: Boolean
        get() { return this.tabDatas[selectedIndex].isTimeMode }
        set(value) { this.tabDatas[selectedIndex].isTimeMode = value }
    var isCulcAvg: Boolean
        get() { return this.tabDatas[selectedIndex].isCulcAvg }
        set(value) { this.tabDatas[selectedIndex].isCulcAvg = value }
    var isUpdateMode: Boolean
        get() { return this.tabDatas[selectedIndex].isUpdateMode }
        set(value) { this.tabDatas[selectedIndex].isUpdateMode = value }
    var startDate: Date?
        get() { return this.tabDatas[selectedIndex].startDate }
        set(value) { this.tabDatas[selectedIndex].startDate = value }
    var backColor: Int
        get() { return this.tabDatas[selectedIndex].backColor }
        set(value) { this.tabDatas[selectedIndex].backColor = value }
    val tabNames: List<String>
        get() { return this.tabDatas.map { it.tabName } }
    val isClearTabAndList: Boolean
        get() { return (this.count == 0.0 &&
                    this.totalCount == 0.0 &&
                    this.dailyTabListSize() == 0) }
    val dailyCountList: List<Double>
        get() { return this.dailyList.map { it.count } }
    val dailyDateList: List<String>
        get() { return this.dailyList.map { it.strDate } }
    val dailyTabList: List<Int>
        get() { return this.dailyList.map { it.tab } }

    init {
        for (i in 0 until MAX_TABS) {
            this.tabDatas.add(TabData())
        }
    }

    // functions

    fun updateCount() {
        val strNewDate = stringFromDate(Date())
        if (strNewDate != this.strDate) {
            val strNextDate = strDateAddDays(this.strDate, 1)
            if (strNextDate != null && strNewDate == strNextDate) {
                this.yesterDayCount = this.count
            } else {
                this.yesterDayCount = 0.0
            }

            this.count = 0.0
            this.strDate = strNewDate
            this.strTime = "-"
        }
    }

    fun updateCount(increment: Double) {
        this.updateCount()

        if (this.isUpdateMode) {
            this.totalCount += -this.count + increment
            this.count = increment
        } else {
            this.count += increment
            this.totalCount += increment
        }

        if (this.incrementHistories.contains(increment)) {
            this.incrementHistories.remove(increment)
        }
        this.incrementHistories.add(0,increment)
        if (4 < this.incrementHistories.size) {
            this.incrementHistories.removeLast()
        }
        this.strTime = stringFromDate(Date(), "HH:mm")

        this.addOrUpdateDailyCount(this.strDate, this.count)
    }

    fun updateCount(tabIndex: Int, increment: Double) {
        val oldSelectedCount = this.selectedIndex
        this.selectedIndex = tabIndex
        this.updateCount(increment)
        this.selectedIndex = oldSelectedCount
    }

    fun addOrUpdateDailyCount(strDate: String, count: Double) {
        var isMatch = false
        for (i in 0 until this.dailyList.size) {
            if (this.dailyList[i].tab == this.selectedIndex
                    && this.dailyList[i].strDate == strDate) {
                this.dailyList[i].count = count
                isMatch = true
                break
            }
        }
        if (!isMatch) {
            this.dailyList.add(
                    DailyData(strDate = strDate, count = count, tab = this.selectedIndex))
            this.tabDatas[this.selectedIndex].dailyList.add(this.dailyList.size - 1)
        }
    }

    fun countClear() {
        this.totalCount -= this.count
        this.count = 0.0
        this.strTime = "-"
        this.addOrUpdateDailyCount(this.strDate, this.count)
     }

    fun countAllClear() {
        this.yesterDayCount = 0.0
        this.totalCount = 0.0
        this.clearAllDailyCount(this.selectedIndex)
        this.updateDailyTabLists()
    }

    fun dailyTabList(idx: Int): DailyData {
        return this.dailyList[this.tabDatas[this.selectedIndex].dailyList[idx]]
    }

    fun dailyTabListCountText(idx: Int): String {
        val count = this.dailyList[this.tabDatas[this.selectedIndex].dailyList[idx]].count
        if (this.isTimeMode) {
            return convStrTime(count.toInt())
        }
        return count.dispString()
    }

    fun dailyCountTextList(): ArrayList<String> {
        var texts = ArrayList<String>()
        for (i in 0 until this.dailyList.size) {
            val count = this.dailyList[i].count
            if (this.tabDatas[this.dailyList[i].tab].isTimeMode) {
                texts.add(convStrTime(count.toInt()))
            } else {
                texts.add(count.dispString())
            }
        }
        return texts
    }

    fun dailyTabListSize(): Int {
        return this.tabDatas[this.selectedIndex].dailyList.size
    }

    fun dailyTabListRemoveAt(idx: Int) {
        val orgIdx = this.tabDatas[this.selectedIndex].dailyList[idx]
        this.dailyList.removeAt(orgIdx)
        this.updateDailyTabLists()
    }

    private fun updateDailyTabLists() {
        for (i in 0 until MAX_TABS) {
            this.tabDatas[i].dailyList.clear()
        }
        for (idx in 0 until this.dailyList.size) {
            if (0 <= this.dailyList[idx].tab && this.dailyList[idx].tab < this.tabDatas.size) {
                this.tabDatas[this.dailyList[idx].tab].dailyList.add(idx)
            }
        }
    }

    //region functions - 集計

    // 全期間のカウント数を返す(合計 or 平均)
    fun countOfAll(): Double {
        if (this.isCulcAvg) {
            return averageOfAll()
        }
        return this.totalCount
    }

    // 全期間の平均カウント数を返す
    fun averageOfAll(): Double {
        if (0 < this.dailyTabListSize()) {
            var totalCount = 0.0
            for (idx in 0 until this.dailyTabListSize()) {
                totalCount += this.dailyTabList(idx).count
            }
            return totalCount / this.dailyTabListSize().toDouble()
        }
        return 0.0
    }

    // 全期間の最小カウント数を返す
    fun minOfAll(): Double {
        if (0 < this.dailyTabListSize()) {
            var minCount = Double.MAX_VALUE
            for (idx in 0 until this.dailyTabListSize()) {
                minCount = Math.min(minCount, this.dailyTabList(idx).count)
            }
            return minCount
        }
        return 0.0
    }

    // 日付情報(開始日・終了日・期間)を返す
    fun dateInfoOfAll() : DateInfo {
        var info = DateInfo()
        if (dailyTabListSize() == 0) {
            return info
        }

        var minDate: Date? = null
        var maxDate: Date? = null
        for (i in 0 until dailyTabListSize()) {
            var date = dateFromString(dailyTabList(i).strDate)
            date?.let {
                if (minDate == null || it.compareTo(minDate) < 0) {
                    minDate = date
                }
                if (maxDate == null || 0 < it.compareTo(maxDate)) {
                    maxDate = date
                }
            }
        }
        if (minDate != null) {
            info.minStrDate = stringFromDate(minDate!!)
            info.maxStrDate = stringFromDate(maxDate!!)
            info.diffDays = diffDays(minDate!!, maxDate!!) + 1
        }
        return info
    }

    // 日付情報(開始日・終了日・期間)
    inner class DateInfo()  {
        var minStrDate: String = ""
        var maxStrDate: String = ""
        var diffDays: Int = 0
    }

    // 全期間の最大カウント数を返す
    fun maxOfAll(): Double {
        var minCount = 0.0
        for (idx in 0 until this.dailyTabListSize()) {
            minCount = Math.max(minCount, this.dailyTabList(idx).count)
        }
        return minCount
    }

    // 今月・先月のカウント数を返す(合計 or 平均)
    fun countOfMonth(): Pair<Double, Double>  {
        val thisMonth = dateSet(today(), day = 1)
        val nextMonth = dateAddMonths(thisMonth,1)
        val beforeMonth = dateAddMonths(thisMonth,-1)
        var thisCount = 0.0
        var thisTimes = 0
        var beforeCount = 0.0
        var beforeTimes = 0
        for (idx in 0 until this.dailyTabListSize()) {
            val date = dateFromString(this.dailyTabList(idx).strDate)!!
            if (0 <= date.compareTo(thisMonth) && date.compareTo(nextMonth) < 0) {
                thisCount += this.dailyTabList(idx).count
                thisTimes += 1
            }
            if (0 <= date.compareTo(beforeMonth) && date.compareTo(thisMonth) < 0) {
                beforeCount += this.dailyTabList(idx).count
                beforeTimes += 1
            }
        }
        // 平均
        if (this.isCulcAvg) {
            if (0 < thisTimes) {
                thisCount /= thisTimes.toDouble()
            }
            if (0 < beforeTimes) {
                beforeCount /= beforeTimes.toDouble()
            }
        }
        return Pair(thisCount, beforeCount)
    }

    // 今週・先週のカウント数を返す(合計 or 平均)
    fun countOfWeek(): Pair<Double, Double>  {
        val thisWeek = dateAddDays(today(), -week(Date()))
        val nextWeek = dateAddDays(thisWeek,7)
        val beforeWeek = dateAddDays(thisWeek,-7)
        var thisCount = 0.0
        var thisTimes = 0
        var beforeCount = 0.0
        var beforeTimes = 0
        for (idx in 0 until this.dailyTabListSize()) {
            val date = dateFromString(this.dailyTabList(idx).strDate)!!
            if (0 <= date.compareTo(thisWeek) && date.compareTo(nextWeek) < 0) {
                thisCount += this.dailyTabList(idx).count
                thisTimes += 1
            }
            if (0 <= date.compareTo(beforeWeek) && date.compareTo(thisWeek) < 0) {
                beforeCount += this.dailyTabList(idx).count
                beforeTimes += 1
            }
        }
        // 平均
        if (this.isCulcAvg) {
            if (0 < thisTimes) {
                thisCount /= thisTimes.toDouble()
            }
            if (0 < beforeTimes) {
                beforeCount /= beforeTimes.toDouble()
            }
        }
        return Pair(thisCount, beforeCount)
    }

    // 月単位のカウント数を返す(合計 or 平均)
    fun countsByMonth(): Pair<ArrayList<Date>, ArrayList<Double>>  {
        var dateArray = ArrayList<Date>()
        var countArray = ArrayList<Double>()
        var timesArray = ArrayList<Int>()
        for (idx in 0 until this.dailyTabListSize()) {
            val monthDate = dateSet(dateFromString(this.dailyTabList(idx).strDate)!!, day = 1)
            val idxDate = dateArray.indexOf(monthDate)
            if (0 <= idxDate) {
                countArray[idxDate] += this.dailyTabList(idx).count
                timesArray[idxDate] += 1
            } else {
                dateArray.add(monthDate)
                countArray.add(this.dailyTabList(idx).count)
                timesArray.add(1)
            }
        }
        // 平均
        if (this.isCulcAvg) {
            for (idx in 0 until countArray.size) {
                if (0 < timesArray[idx]) {
                    countArray[idx] /= timesArray[idx].toDouble()
                }
            }
        }
        return Pair(dateArray, countArray)
    }

    // 週単位のカウント数を返す(合計 or 平均)
    fun countsByWeek(): Pair<ArrayList<Date>, ArrayList<Double>>  {
        var dateArray = ArrayList<Date>()
        var countArray = ArrayList<Double>()
        var timesArray = ArrayList<Int>()
        for (idx in 0 until this.dailyTabListSize()) {
            val date = dateFromString(this.dailyTabList(idx).strDate)!!
            val weekDate = dateAddDays(date, -week(date))
            val idxDate = dateArray.indexOf(weekDate)
            if (0 <= idxDate) {
                countArray[idxDate] += this.dailyTabList(idx).count
                timesArray[idxDate] += 1
            } else {
                dateArray.add(weekDate)
                countArray.add(this.dailyTabList(idx).count)
                timesArray.add(1)
            }
        }
        // 平均
        if (this.isCulcAvg) {
            for (idx in 0 until countArray.size) {
                if (0 < timesArray[idx]) {
                    countArray[idx] /= timesArray[idx].toDouble()
                }
            }
        }
        return Pair(dateArray, countArray)
    }

    //endregion

    // functions - QRコード

    fun createQRData() : Pair<String, Int> {
        var strData = this.strDate + "," +
                this.strTime + "," +
                this.count.dispString() + "," +
                this.yesterDayCount.dispString() + "," +
                this.totalCount.dispString() + ",{"

        val mIdx = this.dailyTabListSize() - 1
        var lines = ArrayList<String>()
        var length = strData.length
        for (idx in 0 until mIdx + 1) {
            val line = this.dailyTabList(mIdx - idx).strDate + "," +
                    this.dailyTabList(mIdx - idx).count.dispString()
            if (MAX_QR_SIZE < length + line.length + 1) {
                break
            }
            lines.add(0, line)
            length += line.length + 1
        }
        strData += lines.joinToString(",") + "}"
        return Pair(strData, lines.size)
    }

    fun importQR(strData: String) : Boolean {
        val array = strData.split(",")
        if (array.size < 6) {
            return false
        }

        var isSuccess = false
        this.strDate = array[0]
        this.strTime = array[1]
        this.count = array[2].toDouble()
        this.yesterDayCount = array[3].toDouble()
        this.totalCount = array[4].toDouble()

        if (array[5] == "{}"){
            return true
        }

        for (i in 0 until ((array.size - 5) / 2)) {
            val pIdx = i * 2 + 5

            var strDate = array[pIdx]
            if (i == 0) {
                if (strDate.startsWith("{")) {
                    strDate = strDate.substring(1)
                } else {
                    break
                }
            }
            var strCount = array[pIdx + 1]
            if (strCount.endsWith("}")) {
                strCount = strCount.substring(0, strCount.length - 1)
                isSuccess = true
            }
            this.addOrUpdateDailyCount(strDate, strCount.toDouble())
        }
        return isSuccess
    }

    // functions - タブ追加/削除

    fun insertTab() {
        if (MAX_TABS <= this.tabCount) {
            return
        }
        this.selectedIndex += 1
        if (this.selectedIndex < this.tabCount) {
            for (i in MAX_TABS - 1 downTo this.selectedIndex + 1) {
                this.copyTab(i - 1, i)
                this.moveAllDailyCount(i - 1, i)
            }
        }
        this.clearTab(this.selectedIndex)
        this.updateDailyTabLists()
        this.tabCount += 1
    }

    fun moveTab(fromIndex: Int, toIndex: Int) {
        this.moveAllDailyCount(fromIndex, Int.MAX_VALUE)
        val tab = this.tabDatas[fromIndex]

        if (fromIndex < toIndex) {
            for (i in fromIndex until toIndex) {
                this.copyTab(i + 1, i)
                this.moveAllDailyCount(i + 1, i)
            }
        } else {
            for (i in fromIndex downTo toIndex + 1) {
                this.copyTab(i - 1, i)
                this.moveAllDailyCount(i - 1, i)
            }
        }

        this.tabDatas[toIndex] = tab
        this.moveAllDailyCount(Int.MAX_VALUE, toIndex)
        this.updateDailyTabLists()
    }

    fun removeTabAndList() {
        if (this.tabCount <= 1) {
            clearTabAndList()
            return
        }
        this.clearAllDailyCount(this.selectedIndex)
        for (i in this.selectedIndex until MAX_TABS - 1) {
            this.copyTab(i + 1, i)
            this.moveAllDailyCount(i + 1, i)
        }
        this.clearTab(MAX_TABS - 1)
        this.updateDailyTabLists()
        this.tabCount -= 1
        if (this.tabCount <= this.selectedIndex) {
            this.selectedIndex = this.tabCount - 1
        }
    }

    private fun clearTabAndList() {
        this.clearAllDailyCount(this.selectedIndex)
        this.clearTab(this.selectedIndex)
        this.updateDailyTabLists()
    }

    private fun copyTab(fromIndex: Int, toIndex: Int) {
        this.tabDatas[toIndex] = this.tabDatas[fromIndex]
    }

    private fun clearTab(index: Int) {
        this.tabDatas[index] = TabData(tabName = (index + 1).toString())
    }

    // 要updateDailyTabLists
    private fun moveAllDailyCount(fromIndex: Int, toIndex: Int) {
        for (i in 0 until this.dailyList.size) {
            if (this.dailyList[i].tab == fromIndex) {
                this.dailyList[i].tab = toIndex
            }
        }
    }

    // 要updateDailyTabLists
    private fun clearAllDailyCount(index: Int) {
        var pointer = 0
        for (i in 0 until this.dailyList.size) {
            if (this.dailyList[pointer].tab == index) {
                this.dailyList.removeAt(pointer)
            } else {
                pointer += 1
            }
        }
    }

    // functions - CSV読込

    fun importCSV(csvArray : List<String>) : Boolean {
        val arrays = stringArrays(csvArray)
        if (arrays.size >= 2) {
            this.dailyList.clear()
            for (idx in 0 until arrays[0].size) {
                val strDate = strDateAddDays(arrays[0][idx], 0) ?: arrays[0][idx]
                val count = convDouble(arrays[1][idx])
                this.dailyList.add(DailyData(strDate = strDate, count = count))
            }
            if (arrays.size >= 3) {
                this.replaceTabs(arrays[2])
                this.tabCount = Math.max(arrays[2].distinct().size, DEFAULT_TABS)
            } else {
                this.replaceTabs(stringList(this.dailyList.size, "1"))
                this.tabCount = DEFAULT_TABS
            }
            this.updateAllTabCountsFromHistory()
            this.updateTimeMode(arrays[1])
            return true
        }
        return false
    }

    // 履歴のタブインデックスと各タブ名を置き換える
    // 既存に存在しないタブは情報を初期化する
    private fun replaceTabs(dailyTabNameList: List<String>) {
        val newTabNames = dailyTabNameList.distinct()
        val newTabCount = newTabNames.size
        var newDiffTabNames = ArrayList<String>()
        for (newTabName in newTabNames) {
            val index = this.tabNames.indexOf(newTabName) ?: -1
            if (0 <= index && index < newTabCount) {
                continue
            }
            newDiffTabNames.add(newTabName)
        }
        for (i in 0 until MAX_TABS) {
            if (!newTabNames.contains(this.tabDatas[i].tabName)) {
                this.clearTab(i)
                if (0 < newDiffTabNames.size) {
                    this.tabDatas[i].tabName = newDiffTabNames.first()
                    newDiffTabNames.removeFirst()
                }
            }
        }
        val dailyTabList = indexList(dailyTabNameList, this.tabNames)
        for (idx in 0 until Math.min(dailyTabList.size, this.dailyList.size)) {
            this.dailyList[idx].tab = dailyTabList[idx]
        }
        this.updateDailyTabLists()
    }

    // 履歴から全タブのカウント数を更新
    private fun updateAllTabCountsFromHistory() {
        val selectedIndex = this.selectedIndex
        for (i in 0 until MAX_TABS) {
            this.selectedIndex = i
            this.updateCountsFromHistory()
        }
        this.selectedIndex = selectedIndex
    }

    // 履歴より現在タブのカウント数を更新
    private fun updateCountsFromHistory() {
        val yesterday = dateAddDays(today(),-1)
        val tomorrow = dateAddDays(today(),1)
        var count = 0.0
        var yesterdayCount = 0.0
        var totalCount = 0.0
        for (idx in 0 until this.dailyTabListSize()) {
            val date = dateFromString(this.dailyTabList(idx).strDate) ?: continue
            if (0 <= date.compareTo(yesterday) && date.compareTo(today()) < 0) {
                yesterdayCount += this.dailyTabList(idx).count
            }
            if (0 <= date.compareTo(today()) && date.compareTo(tomorrow) < 0) {
                count += this.dailyTabList(idx).count
            }
            totalCount += this.dailyTabList(idx).count
        }
        val strDate = stringFromDate(Date())
        if (this.count != count || this.strDate != strDate) {
            this.strTime = "-"
        }
        this.count = count
        this.strDate = strDate
        this.yesterDayCount = yesterdayCount
        this.totalCount = totalCount
    }

    fun updateTimeMode(dailyTabCountList: ArrayList<String>){
        for (tabIdx in 0 until this.tabCount) {
            if (0 < this.tabDatas[tabIdx].dailyList.size) {
                val idx = this.tabDatas[tabIdx].dailyList[0]
                if (dailyTabCountList[idx].contains(":")) {
                    this.tabDatas[tabIdx].isTimeMode = true
                }
            }
        }
    }

    // CSV作成
    fun exportCSV() : ArrayList<String> {
        var arrays = ArrayList<List<String>>()
        arrays.add(this.dailyDateList)
        arrays.add(this.dailyCountTextList())
        arrays.add(nameList(this.dailyTabList, this.tabNames))
        return csvStringArray(arrays)
    }

    // functions - 読み込み・保存

    fun loadVariable() {
        val dataStore = context!!.getSharedPreferences(
                DATA_FILE_NAME,
                Context.MODE_PRIVATE
        )

        this.tabCount = Math.min(dataStore.getInt("tabCount", DEFAULT_TABS), MAX_TABS)

        this.selectedIndex = dataStore.getInt("selectedIndex", 0)
        if (this.tabCount <= this.selectedIndex) {
            this.selectedIndex = 0
        }

        for (i in 0 until this.tabCount) {
            this.tabDatas[i].count = getDoubleFromPreferences(dataStore,"counts${i}", 0.0)
            this.tabDatas[i].yesterDayCount = getDoubleFromPreferences(dataStore,"yesterDayCounts${i}", 0.0)
            this.tabDatas[i].totalCount = getDoubleFromPreferences(dataStore,"totalCounts${i}", 0.0)
            this.tabDatas[i].strDate = dataStore.getString("strDates${i}", "") ?: ""
            this.tabDatas[i].strTime = dataStore.getString("strTimes${i}", "-") ?: ""
            this.tabDatas[i].increment = getDoubleFromPreferences(dataStore,"increments${i}", 1.0)
            this.tabDatas[i].tabName = dataStore.getString("tabNames${i}", (i + 1).toString()) ?: ""
            this.tabDatas[i].incrementHistories = getDoubleListFromPreferences(dataStore, "incrementHistories${i}")
            this.tabDatas[i].isTimeMode = dataStore.getBoolean("isTimeModes${i}", false)
            this.tabDatas[i].isCulcAvg = dataStore.getBoolean("isCulcAvgs${i}", false)
            this.tabDatas[i].isUpdateMode = dataStore.getBoolean("isUpdateModes${i}", false)
            this.tabDatas[i].startDate = getDateFromPreferences(dataStore,"startDates${i}")
            this.tabDatas[i].backColor = dataStore.getInt("backColors${i}", Color.WHITE)
        }
        val dailyDateList = getListFromPreferences(dataStore, "dailyDateList")
        val dailyCountList = getDoubleListFromPreferences(dataStore, "dailyCountList")
        val dailyTabList = getIntListFromPreferences(dataStore, "dailyTabList")
        this.dailyList.clear()
        for (idx in 0 until dailyDateList.size) {
            this.dailyList.add(
                    DailyData(strDate = dailyDateList[idx], count = dailyCountList[idx]))
            if (idx < dailyTabList.size) {
                this.dailyList[idx].tab = dailyTabList[idx]
            }
        }

        // バージョンアップ用
        if (dailyTabList.size == 0) {
            if (this.selectedIndex == 0) {
                this.count = dataStore.getInt("count", 0).toDouble()
                this.yesterDayCount = dataStore.getInt("yesterDayCount", 0).toDouble()
                this.totalCount = dataStore.getInt("totalCount", 0).toDouble()
                this.strDate = dataStore.getString("strDate", "") ?: ""
                this.increment = dataStore.getInt("increment", 1).toDouble()
            }
        }

        this.updateDailyTabLists()
    }

    fun saveVariable() {
        val dataStore = this.context!!.getSharedPreferences(
                DATA_FILE_NAME,
                Context.MODE_PRIVATE
        )
        val editor = dataStore.edit()

        editor.putInt("selectedIndex", this.selectedIndex)
        editor.putInt("tabCount", this.tabCount)

        for (i in 0 until this.tabCount) {
            putDoubleToPreferences(editor, "counts${i}", this.tabDatas[i].count)
            putDoubleToPreferences(editor, "yesterDayCounts${i}", this.tabDatas[i].yesterDayCount)
            putDoubleToPreferences(editor,"totalCounts${i}", this.tabDatas[i].totalCount)
            editor.putString("strDates${i}", this.tabDatas[i].strDate)
            editor.putString("strTimes${i}", this.tabDatas[i].strTime)
            putDoubleToPreferences(editor,"increments${i}", this.tabDatas[i].increment)
            putDoubleListToPreferences(editor, "incrementHistories${i}", this.tabDatas[i].incrementHistories)
            editor.putString("tabNames${i}", this.tabDatas[i].tabName)
            editor.putBoolean("isTimeModes${i}", this.tabDatas[i].isTimeMode)
            editor.putBoolean("isCulcAvgs${i}", this.tabDatas[i].isCulcAvg)
            editor.putBoolean("isUpdateModes${i}", this.tabDatas[i].isUpdateMode)
            putDataToPreferences(editor, "startDates${i}", this.tabDatas[i].startDate)
            editor.putInt("backColors${i}", this.tabDatas[i].backColor)
        }
        putListToPreferences(editor, "dailyDateList", this.dailyDateList)
        putDoubleListToPreferences(editor, "dailyCountList", this.dailyCountList)
        putIntListToPreferences(editor, "dailyTabList", this.dailyTabList)

        editor.apply()
    }

    companion object {
        fun countText(count:Double, isTimeMode:Boolean) : String {
            if (isTimeMode) {
                return convStrTime(count.toInt())
            }
            return count.dispString()
        }
    }
}