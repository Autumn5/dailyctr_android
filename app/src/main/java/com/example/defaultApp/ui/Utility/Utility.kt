package com.example.defaultApp.ui

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import org.json.JSONArray
import java.io.*
import java.lang.Double.*
import java.math.BigDecimal
import java.text.Normalizer
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

//
// Utility 21/2/7(v0.0.6)
//

// Utility methods - date

fun strDateAddDays(strDate: String, days: Int) : String? {
    var date: Date? = dateFromString(strDate)
    if (date != null) {
        return stringFromDate(dateAddDays(date, days))
    }
    return null
}

fun dateAddDays(date: Date, days: Int) : Date {
    val calendar = Calendar.getInstance()
    calendar.time = date
    calendar.add(Calendar.DAY_OF_MONTH, days)
    return calendar.time
}

fun dateAddMonths(date: Date, months: Int) : Date {
    val calendar = Calendar.getInstance()
    calendar.time = date
    calendar.add(Calendar.MONTH, months)
    return calendar.time
}

fun dateSet(date: Date, year: Int? = null, month: Int? = null, day: Int? = null) : Date {
    val calendar = Calendar.getInstance()
    calendar.time = date
    if (year != null) {
        calendar.set(Calendar.YEAR, year!!)
    }
    if (month != null) {
        calendar.set(Calendar.MONTH, month!!)
    }
    if (day != null) {
        calendar.set(Calendar.DAY_OF_MONTH, day!!)
    }
    return calendar.time
}

// 曜日(日曜=0...土曜=6)を取得
fun week(date: Date) : Int {
    val calendar = Calendar.getInstance()
    calendar.time = date
    return calendar.get(Calendar.DAY_OF_WEEK) - 1
}

fun today() : Date {
    val calendar = Calendar.getInstance()
    calendar.time = Date()
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar.time
}

fun dateFromString(strDate: String) : Date? {
    try {
        return SimpleDateFormat("yyyy/MM/dd").parse(strDate)
    } catch (e: ParseException) { }
    return null
}

fun stringFromDate(date: Date) : String {
    return SimpleDateFormat("yyyy/MM/dd").format(date)
}

fun stringFromDate(date: Date, format: String) : String {
    return SimpleDateFormat(format).format(date)
}

fun diffDays(date1: Date, date2: Date) : Int {
    val diff = date2.getTime() - date1.getTime()
    return (diff / 1000 / 86400).toInt()
}

fun diffSeconds(date1: Date, date2: Date) : Int {
    val diff = date2.getTime() - date1.getTime()
    return (diff / 1000).toInt()
}

fun convStrTime(second: Int) : String {
    val hour = second / 3600
    if (0 < hour) {
        return String.format("%02d:%02d:%02d", hour, (second / 60) % 60, second % 60)
    }
    return String.format("%02d:%02d", (second / 60) % 60, second % 60)
}

fun convInt(strTime: String) : Int? {
    var size = 0
    for (str in strTime.split(":")) {
        val num = str.toIntOrNull()
        if (num != null) {
            size = size * 60 + num
        } else {
            return null
        }
    }
    return size
}

fun convDouble(strCount: String) : Double {
    if (strCount.contains(":")) {
        return (convInt(strCount) ?: 0).toDouble()
    }
    return strCount.toDoubleOrNull() ?: 0.0
}

// Utility methods - preferences

fun putListToPreferences(editor: SharedPreferences.Editor, key: String, list: List<String>) {
    val jsonAry = JSONArray()
    for (i in 0 until list.size) {
        jsonAry.put(list[i])
    }
    editor.putString(key, jsonAry.toString())
}

fun putIntListToPreferences(editor: SharedPreferences.Editor, key: String, list: List<Int>) {
    val jsonAry = JSONArray()
    for (i in 0 until list.size) {
        jsonAry.put(list[i])
    }
    editor.putString(key, jsonAry.toString())
}

fun putDoubleListToPreferences(editor: SharedPreferences.Editor, key: String, list: List<Double>) {
    val jsonAry = JSONArray()
    for (i in 0 until list.size) {
        jsonAry.put(list[i])
    }
    editor.putString(key, jsonAry.toString())
}

fun getListFromPreferences(dataStore: SharedPreferences, key: String): ArrayList<String> {
    val list = ArrayList<String>()
    val strJson = dataStore.getString(key, "")
    if (strJson != "") {
        try {
            val jsonAry = JSONArray(strJson)
            for (i in 0 until jsonAry.length()) {
                list.add(jsonAry.getString(i))
            }
        } catch (ex: Exception) {}
    }
    return list
}

fun getIntListFromPreferences(dataStore: SharedPreferences, key: String): ArrayList<Int> {
    val list = ArrayList<Int>()
    val strJson = dataStore.getString(key, "")
    if (strJson != "") {
        try {
            val jsonAry = JSONArray(strJson)
            for (i in 0 until jsonAry.length()) {
                list.add(jsonAry.getInt(i))
            }
        } catch (ex: Exception) {}
    }
    return list
}

fun getDoubleListFromPreferences(dataStore: SharedPreferences, key: String): ArrayList<Double> {
    val list = ArrayList<Double>()
    val strJson = dataStore.getString(key, "")
    if (strJson != "") {
        try {
            val jsonAry = JSONArray(strJson)
            for (i in 0 until jsonAry.length()) {
                list.add(jsonAry.getDouble(i))
            }
        } catch (ex: Exception) {}
    }
    return list
}

fun putDataToPreferences(editor: SharedPreferences.Editor, key: String, date: Date?) {
    editor.putLong(key, date?.time ?: 0)
}

fun getDateFromPreferences(dataStore: SharedPreferences, key: String): Date? {
    val dateTime = dataStore.getLong(key, 0)
    if (0 < dateTime) {
        return Date(dateTime)
    }
    return null
}

fun putDoubleToPreferences(editor: SharedPreferences.Editor, key: String, value: Double) {
    if (value > Math.floor(value)) {
        editor.putLong(key, doubleToRawLongBits(value))
    } else {
        editor.putInt(key, value.toInt())
    }
}

fun getDoubleFromPreferences(dataStore: SharedPreferences,
                             key: String, defaultValue: Double = 0.0): Double {
    if (dataStore.all.containsKey(key)) {
        dataStore.all.getValue(key)?.let {
            if (it is Int) {
                return dataStore.getInt(key, 0).toDouble()
            }
        }
    }
    return longBitsToDouble(dataStore.getLong(key, doubleToRawLongBits(defaultValue)))
}

// Utility methods - file

fun saveStringArray(context: Context, fileName: String, list: List<String>) {
    saveStringArray(File(context.filesDir, fileName).outputStream(), list)
}

fun saveStringArray(outputStream: OutputStream, list: List<String>) {
    outputStream.bufferedWriter().use {
        for (i in 0 until list.size) {
            it.write(list[i] + "\n")
        }
    }
}

fun loadStringArray(context: Context, fileName: String) : ArrayList<String> {
    val file = File(context.filesDir, fileName)
    if (file.exists()) {
        return loadStringArray(file.inputStream())
    }
    return ArrayList<String>()
}

fun loadResourceStringArray(context: Context, fileName: String) : ArrayList<String> {
    val inputStream = context.resources.assets.open(fileName)
    return loadStringArray(inputStream)
}

fun loadStringArray(inputStream: InputStream) : ArrayList<String> {
    var list = ArrayList<String>()
    inputStream.bufferedReader().use {
        var text = trimBOM(it.readText())
        var collection = text.split("\r\n|\r|\n".toRegex())
        for (idx in 0 until collection.size) {
            if (collection[idx] != "") {
                list.add(collection[idx])
            }
        }
    }
    return list
}

fun loadImage(context: Context, uri: Uri, maxWidth: Int) : Bitmap? {
    val matrix = Matrix()
    try {
        val stream = context.contentResolver.openInputStream(uri)!!
        val orientation = ExifInterface(stream).getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED)

        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_180 ->
                matrix.postRotate(180f)
            ExifInterface.ORIENTATION_ROTATE_90 ->
                matrix.postRotate(90f)
            ExifInterface.ORIENTATION_ROTATE_270 ->
                matrix.postRotate(-90f)
        }
    } catch (e: IOException) { }

    var image: Bitmap
    try {
        val stream = context.contentResolver.openInputStream(uri)!!
        image = BitmapFactory.decodeStream(stream)
    } catch (e: IOException) {
        return null
    }

    var width = image.width
    var height = image.height
    if (maxWidth < width) {
        val ratio = maxWidth.toDouble() / width
        width = (width.toDouble() * ratio).toInt()
        height = (height.toDouble() * ratio).toInt()
        image = Bitmap.createScaledBitmap(image, width, height, true)
    }
    return Bitmap.createBitmap(image, 0, 0, width, height, matrix, true)
}

fun loadImage(context: Context, fileName: String) : Bitmap? {
    try {
        var file = File(context.filesDir, fileName)
        val stream = FileInputStream(file)
        return BitmapFactory.decodeStream(stream)
    } catch (e: IOException){
        return null
    }
}

fun saveImage(context: Context, fileName: String, image: Bitmap): Boolean {
    try {
        val file = File(context.filesDir, fileName)
        val outStream = FileOutputStream(file)
        image.compress(Bitmap.CompressFormat.JPEG, 90, outStream)
        outStream.flush()
        outStream.close()
    } catch (e: IOException){
        return false
    }
    return true
}

fun deleteImage(context: Context, fileName: String) {
    try {
        val file = File(context.filesDir, fileName)
        file.delete()
    } catch (e: IOException){ }
}

// Utility methods - string

// BOM除去
fun trimBOM(text: String) : String {
    if (0 < text.length && Integer.toHexString(text[0].toInt()) == "feff") {
        return text.substring(1)
    }
    return text
}

fun filterTexts(texts: ArrayList<String>, searchText: String) : ArrayList<String> {
    if (0 == searchText.length) {
        return texts
    }
    var list = ArrayList<String>()
    for (text in texts) {
        if (text.contains(searchText)) {
            list.add(text)
        }
    }
    return list
}

fun normalize(text: String): String {
    val buf = StringBuffer()
    for (i in 0 until text.length) {
        val code: Char = text[i]
        if (code.toInt() >= 0x3041 && code.toInt() <= 0x3093) {
            buf.append((code.toInt() + 0x60).toChar())
        } else {
            buf.append(code)
        }
    }
    return Normalizer.normalize(buf.toString().toUpperCase(), Normalizer.Form.NFKC)
}

// Utility methods - array

// CSV配列を返す
fun csvStringArray(arrays: List<List<String>>) : ArrayList<String> {
    var array = ArrayList<String>()
    for (row in 0 until arrays[0].size) {
        var lineArray = ArrayList<String>()
        for (idx in 0 until arrays.size) {
            lineArray.add(arrays[idx][row])
        }
        array.add(lineArray.joinToString(","))
    }
    return array
}

fun stringArrays(csvArray: List<String>) : ArrayList<ArrayList<String>> {
    var arrays = ArrayList<ArrayList<String>>()
    for (row in 0 until csvArray.size) {
        if (csvArray[row] == "") { continue }
        val items = csvArray[row].split(",")
        if (row == 0) {
            for (idx in 0 until items.size) {
                arrays.add(ArrayList<String>())
            }
        }
        for (idx in 0 until arrays.size) {
            if (idx < items.size) {
                arrays[idx].add(items[idx])
            } else {
                arrays[idx].add("")
            }
        }
    }
    return arrays
}

fun stringList(intList: List<Int>) : ArrayList<String> {
    var list = ArrayList<String>()
    for (num in intList) {
        list.add(num.toString())
    }
    return list
}

fun stringListFromDubleList(doubleList: List<Double>) : ArrayList<String> {
    var list = ArrayList<String>()
    doubleList.forEach { num ->
        if (num > Math.floor(num)) {
            list.add((BigDecimal(Math.round(num * 100)).divide(100.toBigDecimal())).toString())
        } else {
            list.add(num.toInt().toString())
        }
    }
    return list
}

fun stringList(size: Int, value: String = "") : ArrayList<String> {
    var list = ArrayList<String>()
    for (i in 0 until size) {
        list.add(value)
    }
    return list
}

fun intList(stringList: ArrayList<String>) : ArrayList<Int> {
    var list = ArrayList<Int>()
    for (string in stringList) {
        list.add(string.toInt())
    }
    return list
}

fun intList(size: Int, value: Int = 0) : ArrayList<Int> {
    var list = ArrayList<Int>()
    for (i in 0 until size) {
        list.add(value)
    }
    return list
}

// 名前マスタ配列のインデックス配列を返す
fun indexList(nameList: List<String>, nameMasterList: List<String>,
              defaultValue: Int = -1) : ArrayList<Int> {
    var indexList = ArrayList<Int>()
    for (name in nameList) {
        val idx = nameMasterList.indexOf(name) ?: defaultValue
        indexList.add(idx)
    }
    return indexList
}

// 名前マスタ配列のインデックス配列を名前配列に変換する
fun nameList(nameIndexList: List<Int>, nameMasterList: List<String>,
             defaultValue: String = "") : ArrayList<String> {
    var nameList = ArrayList<String>()
    for (nameIndex in nameIndexList) {
        if (0 <= nameIndex && nameIndex < nameMasterList.size) {
            nameList.add(nameMasterList[nameIndex])
        } else {
            nameList.add(defaultValue)
        }
    }
    return nameList
}

// Utility methods - other

fun simpleAlert(context: Context, title: String, message: String) {
    AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK", { dialog, which -> })
            .show()
}


