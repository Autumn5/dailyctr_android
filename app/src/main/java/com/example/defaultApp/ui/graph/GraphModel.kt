package com.example.defaultApp.ui.graph

import com.example.defaultApp.ui.*
import com.example.defaultApp.ui.home.AppDataModel
import java.util.*
import kotlin.collections.ArrayList

data class GraphPoint(
        val x: Float,
        val y: Float,
        var xLabel: String) {}

class GraphModel {
    companion object {

        fun createGraphArray(dateArray: ArrayList<Date>, countArray: ArrayList<Double>) : ArrayList<GraphPoint> {
            if (dateArray.size == 0) {
                return ArrayList<GraphPoint>()
            }

            var idxArray = mutableListOf<Int>()
            var dateStringArray = ArrayList<String>()
            for (i in 0 until dateArray.size) {
                idxArray.add(i)
                dateStringArray.add(stringFromDate(dateArray[i]))
            }
            idxArray.sortBy {dateArray[it]}
            if (10 < idxArray.size) {
                idxArray = idxArray.takeLast(10).toMutableList()
            }

            val graphArray = ArrayList<GraphPoint>()
            val firstDate = dateArray[idxArray[0]]
            idxArray.forEach { idx ->
                val graphPoint = GraphPoint(
                        diffDays(firstDate, dateArray[idx]).toFloat(),
                        countArray[idx].toFloat(),
                        dateStringArray[idx])
                graphArray.add(graphPoint)
            }
            return graphArray
        }

        fun createGraphDailyArray(dataModel: AppDataModel) : ArrayList<GraphPoint> {
            var dateArray = ArrayList<Date>()
            var countArray = ArrayList<Double>()
            for (i in 0 until dataModel.dailyTabListSize()) {
                dateArray.add(dateFromString(dataModel.dailyTabList(i).strDate)!!)
                countArray.add(dataModel.dailyTabList(i).count)
            }
            return this.createGraphArray(dateArray, countArray)
        }

        fun createGraphMonthArray(dataModel: AppDataModel) : ArrayList<GraphPoint> {
            val pair = dataModel.countsByMonth()
            var graphArray = this.createGraphArray(pair.first, pair.second)
            graphArray.forEach { graph ->
                graph.xLabel = graph.xLabel.substring(0, 7)
            }
            return graphArray
        }

        fun createGraphWeekArray(dataModel: AppDataModel) : ArrayList<GraphPoint> {
            val pair = dataModel.countsByWeek()
            return this.createGraphArray(pair.first, pair.second)
        }

        fun yMaxText(dataModel: AppDataModel, graphArray : ArrayList<GraphPoint>) : String {
            val maxCount = this.maxCount(graphArray)
            if (dataModel.isTimeMode) {
                return convStrTime(maxCount.toInt())
            }
            return maxCount.dispString()
        }

        fun yMinText(dataModel: AppDataModel, graphArray : ArrayList<GraphPoint>) : String {
            val fixMinCount = this.minCount(graphArray)
            if (dataModel.isTimeMode) {
                return convStrTime(fixMinCount.toInt())
            }
            return fixMinCount.dispString()
        }

        fun maxCount(graphArray : ArrayList<GraphPoint>) : Double {
            return (graphArray.map { it.y }.maxOrNull() ?: 0).toDouble()
        }

        fun minCount(graphArray : ArrayList<GraphPoint>) : Double {
            val maxCount = this.maxCount(graphArray)
            val minCount = (graphArray.map { it.y }.minOrNull() ?: 0).toDouble()
            if (minCount < maxCount && (minCount < 0 || maxCount < minCount * 2)) {
                return minCount
            }
            return 0.0
        }
    }
}