package com.example.defaultApp.ui.qr

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.defaultApp.R
import com.example.defaultApp.ui.MAX_QR_SIZE
import com.example.defaultApp.ui.home.AppDataModel
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder

class QrActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr)
        val qrImageView = findViewById<ImageView>(R.id.qrImageView)
        val countInfoTextView = findViewById<TextView>(R.id.countInfoTextView)
        val barcodeEncoder = BarcodeEncoder()
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)

        val dataModel = AppDataModel(this)
        dataModel.loadVariable()
        val qrData = dataModel.createQRData()
        countInfoTextView.text = this.countInfoText(qrData.second, dataModel.dailyTabListSize())

        try {
            val bitmap = barcodeEncoder.encodeBitmap(qrData.first, BarcodeFormat.QR_CODE, 400, 400)
            qrImageView.setImageBitmap(bitmap)
        } catch (e: Exception) { }
    }

    fun countInfoText(size: Int, allSize: Int) : String {
        val prefix: String
        if (allSize == size) {
            prefix = "全"
        } else {
            prefix = "直近"
        }
        return String.format( "%s%d件のデータ(最大%dKB)", prefix, size, MAX_QR_SIZE/1024)
    }

    // actions

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}