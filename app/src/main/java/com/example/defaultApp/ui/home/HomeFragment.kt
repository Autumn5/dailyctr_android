package com.example.defaultApp.ui.home

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.view.View.*
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.example.defaultApp.R
import com.example.defaultApp.ui.*
import com.example.defaultApp.ui.Utility.KeyboardVisibility
import com.example.defaultApp.ui.Utility.TabUtility
import com.example.defaultApp.ui.graph.graphActivity
import com.example.defaultApp.ui.qr.QrActivity
import com.example.defaultApp.ui.qr.QrReadActivity
import com.example.defaultApp.ui.tablist.TablistActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import java.util.*

enum class CountTermType(val rawValue: Int) {
    TERM_DAYLY(0), TERM_WEEKLY(1), TERM_MONTHLY(2)
}

class HomeFragment : Fragment() {

    lateinit var dataModel: AppDataModel
    lateinit var countTextView: TextView
    lateinit var timeTextView: TextView
    lateinit var yesterdayCountTextView: TextView
    lateinit var totalCountTextView: TextView
    lateinit var todayTitleTextView: TextView
    lateinit var yesterdayTitleTextView: TextView
    lateinit var totalTitleTextView: TextView
    lateinit var beforeButton: Button
    lateinit var nextButton: Button
    lateinit var topSpace: Space
    lateinit var rightSpace: Space
    lateinit var countBackView: View
    lateinit var countButton: Button
    lateinit var clearButton: Button
    lateinit var startButton: Button
    lateinit var incrementTitleTextView: TextView
    lateinit var incrementEditText: EditText
    lateinit var incrementBackView: View
    lateinit var tabLayout: TabLayout
    lateinit var tabScrollView: HorizontalScrollView
    lateinit var incrementHistoryView: ConstraintLayout
    lateinit var imageView: ImageView
    var incrementHistoryButtons = ArrayList<Button>()
    var hasFocusIncrementEditText = false
    var isKeyboardVisible = false
    var countTerm = CountTermType.TERM_DAYLY
    val handler = Handler()
    var isHandlerRunning = false
    var isSelectTabByCode = false
    lateinit var tabUtility: TabUtility

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        this.countTextView = root.findViewById<TextView>(R.id.countTextView)
        this.timeTextView = root.findViewById<TextView>(R.id.timeTextView)
        this.yesterdayCountTextView = root.findViewById<TextView>(R.id.yesterdayCountTextView)
        this.totalCountTextView = root.findViewById<TextView>(R.id.totalCountTextView)
        this.todayTitleTextView = root.findViewById<TextView>(R.id.todayTitleTextView)
        this.yesterdayTitleTextView = root.findViewById<TextView>(R.id.yesterdayTitleTextView)
        this.totalTitleTextView = root.findViewById<TextView>(R.id.totalTitleTextView)
        this.countButton = root.findViewById<Button>(R.id.countButton)
        this.startButton = root.findViewById<Button>(R.id.startButton)
        this.clearButton = root.findViewById<Button>(R.id.clearButton)
        this.incrementTitleTextView = root.findViewById<TextView>(R.id.incrementTitleTextView)
        this.incrementEditText = root.findViewById<EditText>(R.id.incrementEditText)
        this.incrementBackView = root.findViewById<View>(R.id.incrementBackView)
        this.tabLayout = root.findViewById<TabLayout>(R.id.tabLayout)
        this.tabScrollView = root.findViewById<HorizontalScrollView>(R.id.tabScrollView)
        this.beforeButton = root.findViewById<Button>(R.id.beforeButton)
        this.nextButton = root.findViewById<Button>(R.id.nextButton)
        this.topSpace = root.findViewById<Space>(R.id.topSpace)
        this.rightSpace = root.findViewById<Space>(R.id.rightSpace)
        this.countBackView = root.findViewById<View>(R.id.countBackView)
        this.incrementHistoryView = root.findViewById<ConstraintLayout>(R.id.incrementHistoryView)
        val incrementHistoryButton1 = root.findViewById<Button>(R.id.incrementHistoryButton1)
        val incrementHistoryButton2 = root.findViewById<Button>(R.id.incrementHistoryButton2)
        val incrementHistoryButton3 = root.findViewById<Button>(R.id.incrementHistoryButton3)
        this.imageView = root.findViewById<ImageView>(R.id.imageView)
        this.dataModel = AppDataModel(this.requireActivity())
        setHasOptionsMenu(true)
        this.tabUtility = TabUtility(this.tabLayout, this.tabScrollView)

        this.incrementHistoryButtons.add(incrementHistoryButton1)
        this.incrementHistoryButtons.add(incrementHistoryButton2)
        this.incrementHistoryButtons.add(incrementHistoryButton3)

        this.tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (isSelectTabByCode) {
                    return
                }
                dataModel.selectedIndex = tabLayout.selectedTabPosition
                if (MAX_SHOW_TABS < tabLayout.tabCount && dataModel.selectedIndex < MAX_SHOW_TABS) {
                    updateTabs()
                }
                updateTabAndCount()
                saveVariable()
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        this.loadVariable()
        this.updateTabAndCount()

        countButton.setOnClickListener {
            this.hideKeyboard()
            this.updateCount(this.getIncrement())
            this.saveVariable()
        }

        startButton.setOnClickListener {
            onClickStartButton()
        }

        clearButton.setOnClickListener {
            onTapClearButton()
        }

        beforeButton.setOnClickListener {
            onTapBeforeButton()
        }

        nextButton.setOnClickListener {
            onTapNextButton()
        }

        this.incrementEditText.setOnFocusChangeListener { v, hasFocus ->
            onFocusIncrementEditText(hasFocus)
        }

        incrementHistoryButton1.setOnClickListener {
            this.onTapIncrementHistoryButton((it as Button).text.toString())
        }

        incrementHistoryButton2.setOnClickListener {
            this.onTapIncrementHistoryButton((it as Button).text.toString())
        }

        incrementHistoryButton3.setOnClickListener {
            this.onTapIncrementHistoryButton((it as Button).text.toString())
        }

        loadImage(this.requireActivity(), "background.jpg")?.let {
            this.imageView.setImageBitmap(it)
            this.imageView.alpha = 0.8f
        }

        val keyboardVisibility = KeyboardVisibility(this.requireActivity())
        keyboardVisibility.setKeyboardVisibilityListener(object: KeyboardVisibility.OnKeyboardVisibilityListener {
            override fun onVisibilityChanged(visible: Boolean) {
                onKeyboardVisibilityChanged(visible)
            }
        })

        this.countBackView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    onTouchView()
                }
                return true
            }
        })

        return root
    }

    override fun onResume() {
        super.onResume()
        this.loadVariable()
        this.updateTabAndCount()
        this.saveVariable()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home_menu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val addTabItem = menu.findItem(R.id.addTabButton)
        addTabItem.isEnabled = (this.dataModel.tabCount < MAX_TABS)
        val timeModeButton = menu.findItem(R.id.tabSettingButton)
        timeModeButton.isEnabled = (this.dataModel.startDate == null)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.addTabButton -> {
                this.dataModel.insertTab()
                this.dataModel.saveVariable()
                this.loadVariable()
                this.updateTabAndCount()
                this.showTabSettingDialog()
                return true
            }
            R.id.delTabButton -> {
                val deleteTabBlock = {
                    this.dataModel.removeTabAndList()
                    this.dataModel.saveVariable()
                    this.loadVariable()
                    this.updateTabAndCount()
                }
                if (this.dataModel.isClearTabAndList) {
                    deleteTabBlock()
                } else {
                    AlertDialog.Builder(this.requireActivity())
                            .setTitle("タブ削除")
                            .setMessage("現在のタブを削除しますか？")
                            .setPositiveButton("削除", { dialog, which ->
                                deleteTabBlock()
                            })
                            .setNegativeButton("キャンセル", { dialog, which ->
                            })
                            .show()
                }
                return true
            }
            R.id.tabSettingButton -> {
                this.showTabSettingDialog()
                return true
            }
            R.id.qrButton -> {
                val intent = Intent(this.requireActivity(), QrActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.qrReadButton -> {
                val intent = Intent(this.requireActivity(), QrReadActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.graphButton -> {
                val intent = Intent(this.requireActivity(), graphActivity::class.java)
                intent.putExtra("COUNT_TERM", this.countTerm)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    // actions

    fun onTapActionBarButton() {
        val intent = Intent(this.requireActivity(), TablistActivity::class.java)
        startActivity(intent)
    }

    fun onTapClearButton() {
        if (this.isHandlerRunning) {
            this.intervalCancel()
            return
        }
        if (0 < this.dataModel.count) {
            this.dataModel.countClear()
            this.updateCount()
            this.saveVariable()
            return
        }

        AlertDialog.Builder(this.requireActivity())
                .setTitle("クリア")
                .setMessage("合計と履歴もクリアしますか？")
                .setPositiveButton("クリア", { dialog, which ->
                    this.dataModel.countAllClear()
                    this.updateCount()
                    this.saveVariable()
                })
                .setNegativeButton("キャンセル", { dialog, which ->
                })
                .show()
    }

    fun onClickStartButton() {
        if (this.isHandlerRunning) {
            this.intervalStop()
            val increment = HomeViewModel.timeCount(this.dataModel)
            this.dataModel.startDate = null
            this.updateCount(increment.toDouble())
            this.saveVariable()
        } else {
            this.dataModel.startDate = Date()
            this.saveVariable()
            this.intervalStart()
        }
    }

    fun onTapBeforeButton() {
        if (this.countTerm == CountTermType.TERM_DAYLY) {
            this.countTerm = CountTermType.TERM_MONTHLY
        } else if (this.countTerm == CountTermType.TERM_MONTHLY) {
            this.countTerm = CountTermType.TERM_WEEKLY
        } else {
            this.countTerm = CountTermType.TERM_DAYLY
        }
        this.updateCount()
    }

    fun onTapNextButton() {
        if (this.countTerm == CountTermType.TERM_DAYLY) {
            this.countTerm = CountTermType.TERM_WEEKLY
        } else if (this.countTerm == CountTermType.TERM_WEEKLY) {
            this.countTerm = CountTermType.TERM_MONTHLY
        } else {
            this.countTerm = CountTermType.TERM_DAYLY
        }
        this.updateCount()
    }

    fun onFocusIncrementEditText(hasFocus: Boolean) {
        this.hasFocusIncrementEditText = hasFocus
        this.updateIncrementHistoryView()
    }

    fun onTapIncrementHistoryButton(text: String) {
        this.incrementEditText.setText(text, TextView.BufferType.NORMAL)
    }

    fun onKeyboardVisibilityChanged(visible: Boolean) {
        this.isKeyboardVisible = visible
        this.updateIncrementHistoryView()
    }

    fun onTouchView() {
        var visible = (if (this.beforeButton.visibility == VISIBLE) GONE else VISIBLE)
        this.beforeButton.visibility = visible
        this.nextButton.visibility = visible
        this.yesterdayTitleTextView.visibility = visible
        this.yesterdayCountTextView.visibility = visible
        this.totalTitleTextView.visibility = visible
        this.totalCountTextView.visibility = visible
        this.todayTitleTextView.visibility = visible
        this.timeTextView.visibility = visible

        val mlp = this.countTextView.layoutParams as ViewGroup.MarginLayoutParams
        val tLayoutParams = this.topSpace.layoutParams as ConstraintLayout.LayoutParams
        val rLayoutParams = this.rightSpace.layoutParams as ConstraintLayout.LayoutParams
        if (visible == VISIBLE) {
            mlp.topMargin = 0
            mlp.width = 0
            tLayoutParams.bottomToTop = R.id.todayTitleTextView
            rLayoutParams.startToEnd = R.id.totalCountTextView
        } else {
            mlp.topMargin = 40
            mlp.width = WRAP_CONTENT
            tLayoutParams.bottomToTop = R.id.countTextView
            rLayoutParams.startToEnd = R.id.countTextView
        }
        this.countTextView.layoutParams = mlp
        this.topSpace.layoutParams = tLayoutParams
        this.rightSpace.layoutParams = rLayoutParams
        this.setCountTextViewText(this.countTextView.text.toString())
    }

    // functions

    fun getIncrement() : Double {
        var increment = 1.0
        try {
            increment = this.incrementEditText.text.toString().toDouble().fix()
        } catch (ex: NumberFormatException) {
        }
        this.incrementEditText.setText(increment.dispString(), TextView.BufferType.NORMAL)
        return increment
    }

    fun updateIncrementHistoryView() {
        if (!this.isKeyboardVisible || !this.hasFocusIncrementEditText
                || this.dataModel.incrementHistories.size < 2) {
            this.incrementHistoryView.visibility = INVISIBLE
            return
        }

        for (i in 0 until this.incrementHistoryButtons.size) {
            if (i + 1 < this.dataModel.incrementHistories.size) {
                this.incrementHistoryButtons[i].text = this.dataModel.incrementHistories[i + 1].dispString()
                this.incrementHistoryButtons[i].visibility = VISIBLE
            } else {
                this.incrementHistoryButtons[i].visibility = INVISIBLE
            }
        }
        this.incrementHistoryView.visibility = VISIBLE
    }

    fun setCountTextViewText(text: String) {
        if (this.beforeButton.visibility == VISIBLE) {
            this.countTextView.textSize = (
                    if (10 <= text.count()) 18F
                    else if (8 <= text.count()) 25F
                    else if (6 <= text.count()) 30F
                    else 40F)
        } else {
            this.countTextView.textSize = 60F
        }
        this.countTextView.ellipsize = null
        this.countTextView.text = text
        this.countTextView.ellipsize = android.text.TextUtils.TruncateAt.END
    }

    fun updateTabs() {
        this.isSelectTabByCode = true
        this.tabUtility.updateTabs(dataModel)
        this.isSelectTabByCode = false
    }

    fun updateCountMode() {
        if (this.dataModel.isTimeMode) {
            this.countButton.visibility = INVISIBLE
            this.startButton.visibility = VISIBLE
            this.incrementEditText.visibility = INVISIBLE
            this.incrementTitleTextView.visibility = INVISIBLE
            this.incrementBackView.visibility = INVISIBLE

            if (this.dataModel.startDate != null) {
                this.intervalStart()
            } else {
                this.intervalStop()
            }
        } else {
            this.countButton.visibility = VISIBLE
            this.startButton.visibility = INVISIBLE
            this.incrementEditText.visibility = VISIBLE
            this.incrementTitleTextView.visibility = VISIBLE
            this.incrementBackView.visibility = VISIBLE
            this.intervalCancel()

            if (this.dataModel.isUpdateMode) {
                this.countButton.text = resources.getString(R.string.title_update)
            } else {
                this.countButton.text = resources.getString(R.string.title_count)
            }
        }
    }

    // functions - tabSettingDialog

    fun showTabSettingDialog() {
        val dialog = HomeTabSettingDialogFragment(this.dataModel)
        dialog.setOnClickHomeTabSettingDialogListener(object :
                HomeTabSettingDialogFragment.OnClickHomeTabSettingDialogListener {
            override fun onTapHomeTabSettingDialogUpdateButton() {
                tabSettingUpdate()
            }
        })
        dialog.show(this.requireActivity().getSupportFragmentManager(), null)
    }

    fun tabSettingUpdate() {
        this.loadVariable()
        this.updateTabAndCount()
    }

    // functions - interval

    fun intervalStart() {
        this.isHandlerRunning = true
        this.startButton.text = "ストップ"
        this.handler.post(this.runObject)
    }

    fun intervalStop() {
        this.isHandlerRunning = false
        this.startButton.text = "スタート"
    }

    fun intervalCancel() {
        if (this.isHandlerRunning) {
            this.dataModel.startDate = null
            this.saveVariable()
            this.intervalStop()
            this.updateCount()
        }
    }

    val runObject = object : Runnable {
        override fun run() {
            if (isHandlerRunning) {
                updateCount()
                handler.postDelayed(this, 1000)
            }
        }
    }

    // functions - override AppDataModel

    fun updateTabAndCount() {
        this.hideKeyboard()
        this.updateCount()
        this.incrementEditText.setText(this.dataModel.increment.dispString(), TextView.BufferType.NORMAL)
        val color = backColor(this.requireActivity(), this.dataModel.backColor)
        this.imageView.setBackgroundColor(color)
        (this.countBackView.getBackground()).setColorFilter(color, PorterDuff.Mode.SRC_IN);
        (this.incrementBackView.getBackground()).setColorFilter(color, PorterDuff.Mode.SRC_IN);
        this.updateCountMode()

        val customView = setActionBarLayout(this.requireActivity(), R.layout.my_action_bar_views, isShowTitle = false)
        val actionBarButton = customView.findViewById<Button>(R.id.actionBarButton)
        actionBarButton.text = sizeSpanText(this.dataModel.tabName, " ▼", 0.7f)
        actionBarButton.setOnClickListener {
            onTapActionBarButton()
        }
    }

    fun updateCount(increment: Double? = null) {
        if (increment != null) {
            this.dataModel.updateCount(increment!!)
        } else {
            this.dataModel.updateCount()
        }
        this.setCountTextViewText(HomeViewModel.todayCountText(this.dataModel))
        this.timeTextView.text = HomeViewModel.strTime(this.dataModel)
        this.yesterdayTitleTextView.text = HomeViewModel.yesterdayTitle(this.countTerm)
        this.totalTitleTextView.text = HomeViewModel.totalTitle(this.dataModel, this.countTerm)

        val sumCountTexts = HomeViewModel.sumCountTexts(this.dataModel, this.countTerm)
        this.yesterdayCountTextView.text = sumCountTexts[0]
        this.totalCountTextView.text = sumCountTexts[1]
    }

    fun loadVariable() {
        this.dataModel.loadVariable()
        updateTabs()
    }

    fun saveVariable() {
        this.dataModel.increment = this.getIncrement()
        this.dataModel.saveVariable()
     }

    // Flagment Utility methods

    fun hideKeyboard() {
        val inputManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(this.view?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

}