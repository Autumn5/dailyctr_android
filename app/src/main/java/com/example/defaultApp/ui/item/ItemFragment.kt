package com.example.defaultApp.ui.item

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.HorizontalScrollView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.defaultApp.R
import com.example.defaultApp.ui.home.AppDataModel
import com.example.defaultApp.ui.*
import com.example.defaultApp.ui.Utility.TabUtility
import com.example.defaultApp.ui.tablist.TablistActivity
import com.google.android.material.tabs.TabLayout

class ItemFragment : Fragment() {
    lateinit var dataModel: AppDataModel
    lateinit var tabLayout: TabLayout
    lateinit var tabScrollView: HorizontalScrollView
    lateinit var tabUtility: TabUtility
    lateinit var adapter: MyItemRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) : View? {
        val root = inflater.inflate(R.layout.fragment_item_list, container, false)
        val listView = root.findViewById<RecyclerView>(R.id.list)
        this.tabLayout = root.findViewById<TabLayout>(R.id.tabLayout)
        this.tabScrollView = root.findViewById<HorizontalScrollView>(R.id.tabScrollView)
        this.dataModel = AppDataModel(this.requireActivity())
        this.tabUtility = TabUtility(this.tabLayout, this.tabScrollView)

        this.loadVariable()
        this.adapter = MyItemRecyclerViewAdapter(this.dataModel)
        listView.adapter = this.adapter
        this.showDivider(listView)

        adapter.setOnItemClickListener(object:MyItemRecyclerViewAdapter.OnItemClickListener {
            override fun onItemClickListener(dataIndex: Int) {
                AlertDialog.Builder(requireActivity())
                    .setTitle("削除")
                    .setMessage("選択行を削除しますか？")
                    .setPositiveButton("削除", { dialog, which ->
                        dataModel.dailyTabListRemoveAt(dataIndex)
                        dataModel.saveVariable()
                        adapter.notifyDataSetChanged()
                    })
                    .setNegativeButton("キャンセル", { dialog, which -> })
                    .show()
            }
        })

        this.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                dataModel.selectedIndex = tabLayout.selectedTabPosition
                updateActionBar()
                adapter.notifyDataSetChanged()
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        return root
    }

    override fun onResume() {
        super.onResume()
        this.loadVariable()
        this.adapter.notifyDataSetChanged()
    }

    // actions

    fun onTapActionBarButton() {
        val intent = Intent(this.requireActivity(), TablistActivity::class.java)
        startActivity(intent)
    }

    // functions

    fun updateTabs() {
        this.tabUtility.updateTabs(dataModel)
    }

    fun updateActionBar() {
        val customView = setActionBarLayout(this.requireActivity(), R.layout.my_action_bar_views, isShowTitle = false)
        val actionBarButton = customView.findViewById<Button>(R.id.actionBarButton)
        actionBarButton.text = sizeSpanText(this.dataModel.tabName, " ▼", 0.7f)
        actionBarButton.setOnClickListener {
            onTapActionBarButton()
        }
    }

    // functions - override AppDataModel

    fun loadVariable() {
        this.dataModel.loadVariable()
        updateTabs()
        updateActionBar()
    }

    // Flagment Utility methods

    fun showDivider(view: RecyclerView) {
        val dividerItemDecoration = DividerItemDecoration(this.activity,
                                        LinearLayoutManager(this.activity).getOrientation())
        view.addItemDecoration(dividerItemDecoration)
    }

}