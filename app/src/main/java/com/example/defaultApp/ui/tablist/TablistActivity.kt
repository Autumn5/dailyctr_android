package com.example.defaultApp.ui.tablist

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.defaultApp.R
import com.example.defaultApp.ui.home.AppDataModel


class TablistActivity : AppCompatActivity() {
    lateinit var searchButton: Button
    lateinit var searchEditTextText: EditText
    lateinit var dataModel: AppDataModel
    lateinit var adapter: TablistItemRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tablist)

        val listView = findViewById<RecyclerView>(R.id.list)
        this.searchEditTextText = findViewById<EditText>(R.id.searchEditText)
        this.dataModel = AppDataModel(this)

        this.loadVariable()
        this.adapter = TablistItemRecyclerViewAdapter(this.dataModel, searchEditTextText.text.toString())
        listView.adapter = this.adapter
        this.showDivider(listView)
        val itemTouchHelper = makeItemTouchHelper()
        itemTouchHelper.attachToRecyclerView(listView)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)

        adapter.setOnItemClickListener(object : TablistItemRecyclerViewAdapter.OnItemClickListener {
            override fun onItemClickListener(dataIndex: Int) {
                onTapRowItem(dataIndex)
            }

            override fun onTapCountButton(dataIndex: Int) {
                onTapRowItemCountButton(dataIndex)
            }

            override fun startDragging(viewHolder: RecyclerView.ViewHolder) {
                itemTouchHelper.startDrag(viewHolder)
            }
        })

        this.searchEditTextText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                adapter.setFilterText(searchEditTextText.text.toString())
            }
        })

    }

    // menu

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.tablist_menu, menu)
        return true
    }

    // actions

    fun onTapRowItem(dataIndex: Int) {
        this.dataModel.selectedIndex = dataIndex
        this.dataModel.saveVariable()
        finish()
    }

    fun onTapRowItemCountButton(dataIndex: Int) {
        this.dataModel.updateCount(dataIndex, dataModel.tabDatas[dataIndex].increment)
        this.dataModel.saveVariable()
        this.adapter.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.addEditButton -> {
                this.adapter.toggleEditMode()
                if (this.adapter.isEditMode) {
                    item.title = resources.getString(R.string.title_done)
                } else {
                    item.title = resources.getString(R.string.title_edit)
                }
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // functions - drag

    fun makeItemTouchHelper(): ItemTouchHelper {
        return ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.START or ItemTouchHelper.END,
                0) {

            override fun onMove(recyclerView: RecyclerView,
                                viewHolder: RecyclerView.ViewHolder,
                                target: RecyclerView.ViewHolder): Boolean {
                val adapter = recyclerView.adapter as TablistItemRecyclerViewAdapter
                val fromPosition = viewHolder.adapterPosition
                val toPosition = target.adapterPosition
                moveItem(adapter.rowList[fromPosition], adapter.rowList[toPosition])
                adapter.notifyItemMoved(fromPosition, toPosition)
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            }

            override fun isLongPressDragEnabled(): Boolean {
                return false
            }
        })
    }

    // functions - override AppDataModel

    fun moveItem(fromPosition:Int, toPosition:Int) {
        if (fromPosition == toPosition) {
            return
        }
        dataModel.moveTab(fromPosition, toPosition)
        dataModel.saveVariable()
        adapter.moveFilterList(fromPosition, toPosition)
        adapter.createRowList()
    }

    fun loadVariable() {
        this.dataModel.loadVariable()
    }

    // Flagment Utility methods

    fun hideKeyboard() {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(findViewById<View>(android.R.id.content).windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun showDivider(view: RecyclerView) {
        val dividerItemDecoration = DividerItemDecoration(this, LinearLayoutManager(this).getOrientation())
        view.addItemDecoration(dividerItemDecoration)
    }

}