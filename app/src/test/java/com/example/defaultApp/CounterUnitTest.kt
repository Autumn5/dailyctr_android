package com.example.defaultApp

import com.example.defaultApp.ui.*
import com.example.defaultApp.ui.graph.GraphModel
import com.example.defaultApp.ui.home.AppDataModel
import com.example.defaultApp.ui.home.*
import com.example.defaultApp.ui.item.*
import com.example.defaultApp.ui.tablist.TablistViewModel
import org.junit.Test

import org.junit.Assert.*

class CounterUnitTest {
    // AppDataModel

    @Test
    fun test_dataModel() {
        var dataModel = AppDataModel(null)

        dataModel.updateCount(1.0)
        assertEquals(dataModel.count, 1.0, 0.0)
        assertEquals(dataModel.totalCount, 1.0, 0.0)

        // 更新モード
        dataModel.isUpdateMode = true
        dataModel.updateCount(5.0)
        assertEquals(dataModel.count, 5.0, 0.0)
        assertEquals(dataModel.totalCount, 5.0, 0.0)

        // クリア
        dataModel.countClear()
        assertEquals(dataModel.count, 0.0, 0.0)
        assertEquals(dataModel.totalCount, 0.0, 0.0)
        assertEquals(dataModel.countOfAll(), 0.0, 0.0)

        // タブ指定カウント
        dataModel = AppDataModel(null)
        dataModel.updateCount(1.0)
        dataModel.updateCount(1.5)
        dataModel.updateCount(1,10.0)
        assertEquals(dataModel.count, 2.5, 0.0)
        assertEquals(dataModel.totalCount, 2.5, 0.0)
        assertEquals(dataModel.tabDatas[1].countText(), "10")
    }

    @Test
    fun test_dataModel_importCSV() {
        var dataModel = AppDataModel(null)

        val csvArray = ArrayList<String>()
        csvArray.add("2020/01/01,1,tab1")
        csvArray.add("2020/01/02,1,tab1")
        assert(dataModel.importCSV(csvArray))
        dataModel.selectedIndex = 0
        assertEquals(dataModel.totalCount, 2.0, 0.0)
        assertEquals(dataModel.tabCount, DEFAULT_TABS)

        // 昨日・今日
        csvArray.clear()
        csvArray.add(stringFromDate(today()) + ",2,tab2")
        csvArray.add(stringFromDate(dateAddDays(today(),-1)) + ",1,tab2")
        csvArray.add(stringFromDate(today()) + ",1,tab1")
        assert(dataModel.importCSV(csvArray))
        dataModel.selectedIndex = 1
        assertEquals(dataModel.count, 2.0,0.0)
        assertEquals(dataModel.yesterDayCount, 1.0, 0.0)

        // 全集計・月集計
        csvArray.clear()
        csvArray.add(stringFromDate(dateSet(today(), day = 1)) + ",2,tab1")
        csvArray.add(stringFromDate(dateAddMonths(today(),  -1)) + ",3,tab1")
        assert(dataModel.importCSV(csvArray))
        dataModel.selectedIndex = 0
        var count = dataModel.countOfAll()
        assertEquals(count, 5.0, 0.0)
        var counts = dataModel.countOfMonth()
        assertEquals(counts.first, 2.0, 0.0)
        assertEquals(counts.second, 3.0, 0.0)

        // 全集計・月集計：平均
        csvArray.clear()
        csvArray.add(stringFromDate(dateSet(today(), day = 1)) + ",2,tab1")
        csvArray.add(stringFromDate(dateSet(today(), day = 2)) + ",4,tab1")
        assert(dataModel.importCSV(csvArray))
        dataModel.isCulcAvg = true
        assertEquals(dataModel.totalCount, 6.0, 0.0)
        assertEquals(dataModel.countOfAll(), 3.0, 0.0)
        counts = dataModel.countOfMonth()
        assertEquals(counts.first, 3.0, 0.0)

        // 日付フォーマット
        csvArray.clear()
        csvArray.add("2020/1/1,2,tab1")
        csvArray.add("2020年元旦,2,tab1")
        assert(dataModel.importCSV(csvArray))
        dataModel.selectedIndex = 0
        assertEquals(dataModel.dailyDateList[0], "2020/01/01")
        assertEquals(dataModel.dailyDateList[1], "2020年元旦")

        // タブ数増減(初期設定)
        dataModel = AppDataModel(null)
        csvArray.clear()
        csvArray.add("2020/1/1,2,tab2")
        assert(dataModel.importCSV(csvArray))

        // タブ数増
        csvArray.clear()
        csvArray.add("2020/1/1,2,tab1")
        csvArray.add("2020/1/1,2,tab2")
        csvArray.add("2020/1/1,2,tab3")
        csvArray.add("2020/1/1,2,tab4")
        csvArray.add("2020/1/1,2,tab5")
        assert(dataModel.importCSV(csvArray))
        assertEquals(dataModel.tabCount, 5)
        assertEquals(dataModel.tabNames[0], "tab2")

        // タブ数減
        csvArray.clear()
        csvArray.add("2020/1/1,2,tab1")
        csvArray.add("2020/1/1,2,tab3")
        csvArray.add("2020/1/1,2,tab5")
        assert(dataModel.importCSV(csvArray))
        assertEquals(dataModel.tabCount, 3)
        assertEquals(dataModel.tabNames[0], "tab5")
        assertEquals(dataModel.tabNames[1], "tab1")
        assertEquals(dataModel.tabNames[2], "tab3")

        // 時間モード
        dataModel = AppDataModel(null)
        csvArray.clear()
        csvArray.add(stringFromDate(today()) + ",62,tab1")
        assert(dataModel.importCSV(csvArray))
        dataModel.isTimeMode = true
        assertEquals(dataModel.dailyTabListCountText(0),"01:02")

        // 時間モード解析
        dataModel = AppDataModel(null)
        csvArray.clear()
        csvArray.add("2020/01/02,01:59,tab1")
        assert(dataModel.importCSV(csvArray))
        assertEquals(dataModel.isTimeMode, true)
    }

    @Test
    fun test_dataModel_exportCSV() {
        val dataModel = AppDataModel(null)
        var csvArray = ArrayList<String>()
        csvArray.add("2020/01/01,1,tab1")
        csvArray.add("2020/01/02,1.01,tab1")
        csvArray.add("2020/01/02,1.99,tab1")
        csvArray.add("2020/01/02,01:59,tabt")
        csvArray.add("2020/01/02,00:01,tabt")
        assert(dataModel.importCSV(csvArray))

        val exportCsvArray = dataModel.exportCSV()
        assertEquals(csvArray[0], exportCsvArray[0])
        assertEquals(csvArray[1], exportCsvArray[1])
        assertEquals(csvArray[2], exportCsvArray[2])
        assertEquals(csvArray[3], exportCsvArray[3])
        assertEquals(csvArray[4], exportCsvArray[4])
    }

    @Test
    fun test_dataModel_createQR() {
        val dataModel = AppDataModel(null)

        dataModel.strDate = "2020/01/02"
        dataModel.strTime = "10:11"
        dataModel.count = 1.0
        dataModel.yesterDayCount = 2.0
        dataModel.totalCount = 3.0
        var qrData = dataModel.createQRData()
        assertEquals(qrData.first,"2020/01/02,10:11,1,2,3,{}")

        dataModel.addOrUpdateDailyCount("2020/02/03",4.0)
        dataModel.addOrUpdateDailyCount("2020/03/04",5.0)
        qrData = dataModel.createQRData()
        assertEquals(qrData.first,"2020/01/02,10:11,1,2,3,{2020/02/03,4,2020/03/04,5}")
    }

    @Test
    fun test_dataModel_importQR() {
        val dataModel = AppDataModel(null)
        dataModel.addOrUpdateDailyCount("2020/03/02",10.0)
        dataModel.addOrUpdateDailyCount("2020/03/04",20.0)

        assert(dataModel.importQR("2020/01/02,10:11,1,2,3,{2020/02/03,4,2020/03/04,5}"))
        assertEquals(dataModel.strDate, "2020/01/02")
        assertEquals(dataModel.strTime, "10:11")
        assertEquals(dataModel.count, 1.0,0.0)
        assertEquals(dataModel.yesterDayCount, 2.0,0.0)
        assertEquals(dataModel.totalCount, 3.0,0.0)
        assertEquals(dataModel.countOfAll(), 3.0, 0.0)
        assertEquals(dataModel.dailyTabList(2).strDate,"2020/02/03")
        assertEquals(dataModel.dailyTabList(2).count,4.0,0.0)
        assertEquals(dataModel.dailyTabList(1).strDate,"2020/03/04")
        assertEquals(dataModel.dailyTabList(1).count,5.0,0.0)

        dataModel.isCulcAvg = true
        dataModel.addOrUpdateDailyCount("2020/03/05",2.0)
        assertEquals(dataModel.countOfAll(), 5.25, 0.0)
    }

    @Test
    fun test_dataModel_insertTab() {
        val dataModel = AppDataModel(null)
        dataModel.selectedIndex = 2
        dataModel.addOrUpdateDailyCount("2020/03/02",10.0)
        dataModel.tabName = "test"

        dataModel.selectedIndex = 0
        dataModel.insertTab()
        dataModel.insertTab()
        dataModel.insertTab()
        assertEquals(dataModel.tabCount, DEFAULT_TABS + 3)
        dataModel.selectedIndex = 5
        assertEquals(dataModel.dailyTabListSize(), 1)
        assertEquals(dataModel.tabName, "test")

        dataModel.selectedIndex = 0
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        assertEquals(dataModel.tabCount, 3)
        dataModel.selectedIndex = 2
        assertEquals(dataModel.dailyTabListSize(), 1)
        assertEquals(dataModel.tabName, "test")
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        dataModel.removeTabAndList()
        assertEquals(dataModel.tabCount, 1)
        assertEquals(dataModel.tabName, "1")

        for (i in dataModel.tabCount .. MAX_TABS) {
            dataModel.insertTab()
        }
        assertEquals(dataModel.tabCount, MAX_TABS)
    }

    @Test
    fun test_dataModel_moveTab() {
        lateinit var dataModel : AppDataModel
        fun reset(){
            dataModel = AppDataModel(null)
            dataModel.selectedIndex = 0
            dataModel.addOrUpdateDailyCount("2020/01/01",1.0)
            dataModel.tabName = "tab1"
            dataModel.selectedIndex = 1
            dataModel.addOrUpdateDailyCount("2020/02/01",2.0)
            dataModel.tabName = "tab2"
            dataModel.selectedIndex = 2
            dataModel.addOrUpdateDailyCount("2020/03/01",3.0)
            dataModel.tabName = "tab3"
            dataModel.selectedIndex = 3
            dataModel.addOrUpdateDailyCount("2020/04/01",4.0)
            dataModel.tabName = "tab4"
        }

        reset()
        dataModel.moveTab(0,2)
        dataModel.selectedIndex = 0
        assertEquals(dataModel.tabName, "tab2")
        assertEquals(dataModel.dailyTabList(0).count,2.0,0.0)
        dataModel.selectedIndex = 1
        assertEquals(dataModel.tabName, "tab3")
        assertEquals(dataModel.dailyTabList(0).count,3.0,0.0)
        dataModel.selectedIndex = 2
        assertEquals(dataModel.tabName, "tab1")
        assertEquals(dataModel.dailyTabList(0).count,1.0,0.0)
        dataModel.selectedIndex = 3
        assertEquals(dataModel.tabName, "tab4")
        assertEquals(dataModel.dailyTabList(0).count,4.0,0.0)

        reset()
        dataModel.moveTab(3,1)
        dataModel.selectedIndex = 0
        assertEquals(dataModel.tabName, "tab1")
        assertEquals(dataModel.dailyTabList(0).count,1.0,0.0)
        dataModel.selectedIndex = 1
        assertEquals(dataModel.tabName, "tab4")
        assertEquals(dataModel.dailyTabList(0).count,4.0,0.0)
        dataModel.selectedIndex = 2
        assertEquals(dataModel.tabName, "tab2")
        assertEquals(dataModel.dailyTabList(0).count,2.0,0.0)
        dataModel.selectedIndex = 3
        assertEquals(dataModel.tabName, "tab3")
        assertEquals(dataModel.dailyTabList(0).count,3.0,0.0)
    }

    // HomeViewModel

    @Test
    fun test_HomeViewModel_count() {
        // 今日・昨日・合計
        var dataModel = AppDataModel(null)
        val csvArray = ArrayList<String>()
        csvArray.add("2020/01/01,60,tab1")
        csvArray.add(stringFromDate(dateAddDays(today(),-1)) + ",2,tab1")
        csvArray.add(stringFromDate(today()) + ",3,tab1")
        assert(dataModel.importCSV(csvArray))

        assertEquals(HomeViewModel.todayCountText(dataModel), "3")
        var sumCountTexts = HomeViewModel.sumCountTexts(dataModel, CountTermType.TERM_DAYLY)
        assertEquals(sumCountTexts[0],"2")
        assertEquals(sumCountTexts[1],"65")
        assertEquals(HomeViewModel.yesterdayTitle(CountTermType.TERM_DAYLY),"昨日")
        assertEquals(HomeViewModel.totalTitle(dataModel, CountTermType.TERM_DAYLY),"合計")

        dataModel.isTimeMode = true
        assertEquals(HomeViewModel.todayCountText(dataModel), "00:03")
        sumCountTexts = HomeViewModel.sumCountTexts(dataModel, CountTermType.TERM_DAYLY)
        assertEquals(sumCountTexts[0],"00:02")
        assertEquals(sumCountTexts[1],"01:05")

        // 今月・先月
        dataModel = AppDataModel(null)
        csvArray.clear()
        csvArray.add(stringFromDate(dateSet(today(), day = 1)) + ",2,tab1")
        csvArray.add(stringFromDate(dateSet(today(), day = 2)) + ",3,tab1")
        csvArray.add(stringFromDate(dateAddMonths(today(),  -1)) + ",1,tab1")
        assert(dataModel.importCSV(csvArray))

        sumCountTexts = HomeViewModel.sumCountTexts(dataModel, CountTermType.TERM_MONTHLY)
        assertEquals(sumCountTexts[0],"5")
        assertEquals(sumCountTexts[1],"1")
        assertEquals(HomeViewModel.yesterdayTitle(CountTermType.TERM_MONTHLY),"今月")
        assertEquals(HomeViewModel.totalTitle(dataModel, CountTermType.TERM_MONTHLY),"先月")
    }

    // GraphModel

    @Test
    fun test_GraphModel_createGraphArray() {
        var dataModel = AppDataModel(null)
        var graphArray = GraphModel.createGraphDailyArray(dataModel)
        assertEquals(graphArray.size, 0)

        dataModel.addOrUpdateDailyCount("2020/03/02", 10.0)
        dataModel.addOrUpdateDailyCount("2020/03/07", 20.0)
        dataModel.addOrUpdateDailyCount("2020/03/08", 40.0)
        dataModel.addOrUpdateDailyCount("2020/04/01", 30.0)

        graphArray = GraphModel.createGraphDailyArray(dataModel)
        assertEquals(graphArray.size, 4)
        assertEquals(graphArray[0].y, 10f)
        assertEquals(graphArray[1].x, 5f)
        assertEquals(GraphModel.yMaxText(dataModel, graphArray), "40")
        assertEquals(GraphModel.yMinText(dataModel, graphArray), "0")

        graphArray = GraphModel.createGraphWeekArray(dataModel)
        assertEquals(graphArray.size, 3)
        assertEquals(graphArray[0].y, 30f)
        assertEquals(graphArray[1].x, 7f)

        graphArray = GraphModel.createGraphMonthArray(dataModel)
        assertEquals(graphArray.size, 2)
        assertEquals(graphArray[0].y, 70f)
        assertEquals(graphArray[1].x, 31f)

        // MIN/MAX
        dataModel = AppDataModel(null)
        dataModel.addOrUpdateDailyCount("2020/03/01", 100.0)
        dataModel.addOrUpdateDailyCount("2020/03/02", 60.0)
        graphArray = GraphModel.createGraphDailyArray(dataModel)
        assertEquals(GraphModel.yMaxText(dataModel, graphArray), "100")
        assertEquals(GraphModel.yMinText(dataModel, graphArray), "60")

        dataModel.addOrUpdateDailyCount("2020/03/03", 40.0)
        graphArray = GraphModel.createGraphDailyArray(dataModel)
        assertEquals(GraphModel.yMinText(dataModel, graphArray), "0")

    }

    // TablistViewModel

    @Test
    fun test_TablistViewModel() {
        val dataModel = AppDataModel(null)
        dataModel.addOrUpdateDailyCount("2021/02/01", 50.0)
        dataModel.addOrUpdateDailyCount(stringFromDate(dateSet(today(), month = 2, day = 1)), 100.0)

        var tab = dataModel.tabDatas[0]
        assertEquals(TablistViewModel.firstRowCountText(tab, dataModel),"100")
        assertEquals(TablistViewModel.firstRowDateText(tab, dataModel),"03/01")

        // 今日
        dataModel.selectedIndex = 1
        dataModel.addOrUpdateDailyCount(stringFromDate(today()), 60.5)
        dataModel.selectedIndex = 0
        tab = dataModel.tabDatas[1]
        assertEquals(TablistViewModel.firstRowCountText(tab, dataModel),"60.5")
        assertEquals(TablistViewModel.firstRowDateText(tab, dataModel),"今日")

    }

    // ItemViewModel

    @Test
    fun test_ItemViewModel() {
        val dataModel = AppDataModel(null)
        var dateInfo = dataModel.dateInfoOfAll()
        assertEquals(ItemViewModel.headerCountText(dataModel, dateInfo), "")
        assertEquals(ItemViewModel.headerDateTermText(dateInfo),"〜")
        assertEquals(AppDataModel.countText(dataModel.totalCount, dataModel.isTimeMode),"0")
        assertEquals(AppDataModel.countText(dataModel.averageOfAll(), dataModel.isTimeMode),"0")
        assertEquals(AppDataModel.countText(dataModel.minOfAll(), dataModel.isTimeMode),"0")
        assertEquals(AppDataModel.countText(dataModel.maxOfAll(), dataModel.isTimeMode),"0")

        var csvArray = ArrayList<String>()
        csvArray.add("2021/01/01,50,tab1")
        csvArray.add("2021/02/01,100,tab1")
        csvArray.add("2021/03/01,200,tab2")
        assert(dataModel.importCSV(csvArray))
        dateInfo = dataModel.dateInfoOfAll()
        assertEquals(ItemViewModel.headerCountText(dataModel, dateInfo), "2件(平均16日/件)")
        assertEquals(ItemViewModel.headerDateTermText(dateInfo),"2021/01/01〜2021/02/01")
        assertEquals(AppDataModel.countText(dataModel.totalCount, dataModel.isTimeMode),"150")
        assertEquals(AppDataModel.countText(dataModel.averageOfAll(), dataModel.isTimeMode),"75")
        assertEquals(AppDataModel.countText(dataModel.minOfAll(), dataModel.isTimeMode),"50")
        assertEquals(AppDataModel.countText(dataModel.maxOfAll(), dataModel.isTimeMode),"100")

        dataModel.isTimeMode = true
        assertEquals(AppDataModel.countText(dataModel.totalCount, dataModel.isTimeMode),"02:30")
        assertEquals(AppDataModel.countText(dataModel.averageOfAll(), dataModel.isTimeMode),"01:15")
        assertEquals(AppDataModel.countText(dataModel.minOfAll(), dataModel.isTimeMode),"00:50")
        assertEquals(AppDataModel.countText(dataModel.maxOfAll(), dataModel.isTimeMode),"01:40")
     }
}