package com.example.defaultApp

import com.example.defaultApp.ui.*
import org.junit.Test

import org.junit.Assert.*
import java.text.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UtilityUnitTest {
    @Test
    fun test_strDateAddDays() {
        val strDate = strDateAddDays("2021/1/1", 1)
        assertEquals("2021/01/02", strDate)
    }

    @Test
    fun test_stringFromDate() {
        val formatter = SimpleDateFormat("yyyy/MM/dd HH:mm")

        val sDate = formatter.parse("2020/01/01 00:01")
        assertEquals("2020/01/01", stringFromDate(sDate))

        val eDate = formatter.parse("2020/12/31 23:59")
        assertEquals("2020/12/31", stringFromDate(eDate))
    }

    @Test
    fun test_indexList() {
        val strArray = ArrayList<String>()
        strArray.add("tab1")
        strArray.add("tab3")
        strArray.add("tab5")
        val strMasterArray = ArrayList<String>()
        strMasterArray.add("tab1")
        strMasterArray.add("tab2")
        strMasterArray.add("tab3")
        val array = indexList(strArray, strMasterArray)
        assertEquals(array[0], 0)
        assertEquals(array[1], 2)
        assertEquals(array[2], -1)
    }

}