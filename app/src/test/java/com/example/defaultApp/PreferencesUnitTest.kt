package com.example.defaultApp

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.defaultApp.ui.home.AppDataModel
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PreferencesUnitTest {

    @Test
    fun test_sharedPreferences() {
        // 空取得/保存
        val context = ApplicationProvider.getApplicationContext<Context>()
        var dataModel = AppDataModel(context)
        dataModel.loadVariable()
        Assert.assertEquals(dataModel.selectedIndex, 0)
        dataModel.saveVariable()

        // 更新取得
        dataModel.selectedIndex = 1
        dataModel.saveVariable()
        dataModel.selectedIndex = 2
        dataModel.loadVariable()
        Assert.assertEquals(dataModel.selectedIndex, 1)
    }
}